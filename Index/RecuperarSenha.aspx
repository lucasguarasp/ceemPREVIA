﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RecuperarSenha.aspx.cs" Inherits="Index_RecuperarSenha" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CEEM - Recuperar senha</title>

    <!-- core CSS -->
    <link href="css/bootstrap_for_index.min.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/animate_for_index.min.css" rel="stylesheet" />
    <link href="css/main_for_index.css" rel="stylesheet" />
    <link href="css/style_for_index.css" rel="stylesheet" />
    <link href="css/responsive_for_index.css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>
<body>

    <form id="form1" runat="server">

        <div id="background" class="container">
            <div class="row main">
                <div class="panel-heading">
                    <div class="panel-title text-center">
                        <a class="no-text-decoration" href="index.html">
                            <img src="images/Logo_vFinal_v2.png" width="250" height="80" /></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                        <div class="panel panel-default">
                            <div class="panel-body">

                                <div class="text-center">
                                    <div>
                                        <h4><i class="fa fa-lock fa-4x"></i></h4>
                                        <h3 class="text-center">Esqueceu a senha?</h3>
                                        <p>Você pode recuperar sua senha aqui.</p>
                                    </div>
                                    

                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope color-blue"></i></span>                                               
                                                <asp:TextBox ID="txtEmail" runat="server" placeholder="Email" class="form-control" type="email" required=""></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="form-group">                                           
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user color-blue"></i></span>
                                                <asp:TextBox ID="txtCPFCNPJ" runat="server" placeholder="CPF / CNPJ" class="form-control" required=""></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">                                          
                                            <asp:Button ID="btnRecuperarSenha" runat="server" Text="Recuperar Senha" class="btn btn-lg btn-primary btn-block" />                                       
                                        </div>
                                   
                                </div>

                                <a href="Login.aspx" class="btn-default">
                                    <h2><i class="glyphicon glyphicon-circle-arrow-left"></i></h2>
                                </a>
                                <!--
                            <div class="text-left">
                              <a href="login.html">Lembrou a senha?</a>
                            </div>
-->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 text-center">
                        <p>©2017 Todos os direitos reservados</p>
                    </div>
                </div>
            </div>
        </div>

    </form>


</body>
</html>
