Push no projeto
--------------------
Para fazer o push das alterações nesse repositório é necessario criar uma branch.
O padrão dos nomes das branches é **(seu nome)/(telas modificadas separadas por barra)** em letras minúsculas
- Caso a mudança não seja diretamentamente o nome das telas afetadas devem ser também citadas
- Ser o mais específico possível, porém, caso uma mudança em uma tela "pai" for feita, as "filhas" não precisam ser sitadas
- Os nomes das telas devem ser os mesmos dos arquivos html usados
- Exemplo: **maicon/login/recuperar_senha/dashboard_usuario_comum**

> 1. Após as mudanças verificar as telas e arquivos que foram modificados (o comando `git status` pode ajudar a encontrá-las)
> 2. Criar uma branch com o nome padronizado acima: `git checkout -b (nome da branch)`
> 3. Adicionar as mudanças para posterior commit: `git add (arquivos a serem sincronizados no repositório remoto Gitlab)`
> 4. Fazer o commit com uma mensagem sugestiva: `git commit -m "(mensagem explicativa da mudança)"`
> 5. Fazer o push para a branch da mudança: `git push origin (nome da branch)`