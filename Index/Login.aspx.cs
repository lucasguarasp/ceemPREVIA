﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index_Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnEntrar_Click(object sender, EventArgs e)
    {
        lblMensagem.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);


        DataSet ds = UsuarioBD.SelectLOGIN(txtUserName.Text, txtPassword.Text);
        if (ds.Tables[0].Rows.Count == 1)
        {
            Session["nome"] = ds.Tables[0].Rows[0]["usu_nome"].ToString();
            Session["perfil"] = ds.Tables[0].Rows[0]["tip_tipo_usuario_tip_codigo"].ToString();
            Session["data"] = ds.Tables[0].Rows[0]["usu_dt_cadastro"].ToString();
            int perfil = Convert.ToInt32(Session["perfil"]);
            if (perfil == 1) { Response.Redirect("../pages/administrador/Index.aspx"); }
            if (perfil == 2) { Response.Redirect("../pages/suporte/Index.aspx"); }
            if (perfil == 3) { Response.Redirect("../pages/entidade/Index.aspx"); }
            if (perfil == 4) { Response.Redirect("../pages/comum/Index.aspx"); }
        }
        else {
            //Response.Redirect("Erro.aspx");
            lblMensagem.Text = "<div class='alert span4 alert-danger'>Usuário ou Senha inválido</div>";
        }
    }


}