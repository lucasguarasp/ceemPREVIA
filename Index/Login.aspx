﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Index_Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>CEEM - Login</title>

    <!-- core CSS -->
    <link href="css/bootstrap_for_index.min.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/animate_for_index.min.css" rel="stylesheet" />
    <link href="css/main_for_index.css" rel="stylesheet" />
    <link href="css/style_for_index.css" rel="stylesheet" />
    <link href="css/responsive_for_index.css" rel="stylesheet" />
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css' />
</head>
<body>

    <form id="form1" runat="server">

        <div id="background" class="container">
            <div class="row main">
                <div class="panel-container">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <a class="no-text-decoration" href="index.html">
                                <img src="images/Logo_vFinal_v2.png" width="250" height="80" /></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                            <div class="panel panel-default">
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label for="username" class="control-label">Usuário</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                            <asp:TextBox ID="txtUserName" runat="server" class="form-control" placeholder="Usuário / Email" required=""></asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password" class="control-label">Senha</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                            <asp:TextBox ID="txtPassword" runat="server" class="form-control" placeholder="Senha" required="" type="password"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:CheckBox ID="checkboxSenha" runat="server" Text="Lembrar senha" class="left" type="checkbox" />
                                    </div>
                                    <div class="form-group ">
                                        <asp:Button ID="btnEntrar" runat="server" Text="Entrar" class="btn btn-success btn-lg btn-block login-button" OnClick="btnEntrar_Click" />
                                        <div class="col-lg-12">
                                            <asp:Label ID="lblMensagem" runat="server" Visible="false" ></asp:Label>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="form-group center">
                                            <a href="RecuperarSenha.aspx">Esqueceu a senha?</a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="text-center">
                                <h4><a href="cadastro.html">Não tem cadastro?</a></h4>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 text-center">
                        <p>©2017 Todos os direitos reservados</p>
                    </div>
                </div>
            </div>
        </div>

         <script type="text/javascript">
             function HideLabel() {
                 var seconds = 5;
                 setTimeout(function () {
                     document.getElementById("<%=lblMensagem.ClientID %>").style.display = "none";
             }, seconds * 1000);
             };
    </script>

    </form>

   

</body>
</html>
