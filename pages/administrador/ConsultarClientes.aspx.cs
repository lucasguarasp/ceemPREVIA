﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_adm_ConsultarSensores : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CarregarGrid();
        }
    }

    private void CarregarGrid()
    {

        DataSet ds = ClienteDB.SelectAll();
        int qtd = ds.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            gridClientes.DataSource = ds.Tables[0].DefaultView;
            gridClientes.DataBind();
            gridClientes.Visible = true;
            lbl.Text = qtd + " Clientes";
        }
        else
        {
            gridClientes.Visible = false;
            lbl.Text = "Nenhum cliente encontrado...";
        }
    }
}