﻿<%@ Page Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="ConsultarSensores.aspx.cs" Inherits="pages_adm_ConsultarSensores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="box box-info">

        <div class="box-header">
            <h3 class="box-title">Sensores</h3>
        </div>

        <div class="box-body">
            <div style="overflow-x:auto;">
            <asp:GridView ID="gridSensores" runat="server" CssClass="table table-responsive" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">

                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                <Columns>
                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/bower_components/Ionicons/png/512/search.png" ShowSelectButton="True">
                        <ControlStyle Height="16px" Width="16px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="sen_id" HeaderText="Id Sensor" />
                    <asp:BoundField DataField="local_sensor" HeaderText="Local sensor" />
                    <asp:BoundField DataField="loc_descricao_sensor" HeaderText="Descrição local sensor" />
                    <asp:BoundField DataField="loc_dt_instalacao_sensor" HeaderText="Data instalação sensor" />
                    <asp:BoundField DataField="equ_id" HeaderText="Id Equipamento" />
                    <asp:BoundField DataField="loc_equipamento" HeaderText="Local equipamento" />
                    <asp:BoundField DataField="loc_descricao_equipamento" HeaderText="Descrição local sensor" />
                    <asp:BoundField DataField="loc_dt_instalacao_equipamento" HeaderText="Data instalação equipamento" />
                    <asp:BoundField DataField="cli_codigo" HeaderText="Código cliente" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>

            <asp:Label ID="lbl" runat="server"></asp:Label>
            </div>
        </div>
    </div>

</asp:Content>
