﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_CadastrarAdmSup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnCadastrar_Click(object sender, EventArgs e)
    {

        lblMsg.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);
        

        if (txtNome.Text != "" && txtLogin.Text != "" && txtSenha.Text != "" && txtSenha.Text == txtRepitaSenha.Text)
        {

            Usuario usuario = new Usuario();
            usuario.Nome = txtNome.Text;
            usuario.Login = txtLogin.Text;
            usuario.Senha = txtSenha.Text;
            usuario.Status = false;
            usuario.DataCadastro = DateTime.Now;

            TipoUsuario tip = new TipoUsuario();
            tip.IdTipoUsuario = Convert.ToInt32(ddlTipo.SelectedValue);
            usuario.FkTipoUsuario = tip;


            switch (UsuarioBD.Insert(usuario))
            {
                case 0:
                    lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Cadastrado com sucesso</div>";
                    txtNome.Text = "";
                    txtLogin.Text = "";
                    txtSenha.Text = "";
                    txtRepitaSenha.Text = "";
                    break;
                case -2:
                    lblMsg.Text = "</br></br> <div class='alert alert-danger'>Cadastro não pôde ser concluído</div>";
                    txtNome.Text = "";
                    txtLogin.Text = "";
                    txtSenha.Text = "";
                    txtRepitaSenha.Text = "";
                    break;
            }

        }
        else if (txtSenha.Text != txtRepitaSenha.Text)
        {
            lblMsg.Text = "</br></br> <div class='alert alert-danger'>Senhas diferentes!!!</div>";
        }else
        {
            lblMsg.Text = "</br></br> <div class='alert alert-danger'>Preencha os campos</div>";
        }



    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        TipoUsuario tipo = new TipoUsuario();
        tipo.Descricao = txtPerfil.Text;


        do
        {
            if (txtSenha.Text != txtRepitaSenha.Text)
            {
                lblMsg.Text = "</br> </br><div class='alert span4 alert-danger'>Senhas diferentes</div>";
                txtSenha.Text = "";
                txtRepitaSenha.Text = "";
            }
            else
            {
                lblMsg.Text = "</br> </br><div class='alert span4 alert-danger'>Digite uma senha</div>";
            }
        } while (txtSenha.Text == "");


        switch (UsuarioBD.Insert(tipo))
        {
            case 0:
                lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Tipo de usuario cadastrado com sucesso</div>";
                txtPerfil.Text = "";
                break;
            case -2:
                lblMsg.Text = "</br></br> <div class='alert alert-danger'>Tipo de usuario não pôde ser concluído</div>";
                break;
        }

    }



}