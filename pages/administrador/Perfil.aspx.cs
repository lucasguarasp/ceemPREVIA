﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_Perfil : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)

    {

        //txtNome.Text = Session["nome"].ToString();
    }

    protected void btnEditarPerfil_Click(object sender, EventArgs e)
    {
        txtNome.Enabled = true;
        txtLogin.Enabled = true;
        txtSenha.Enabled = true;
        ddlPerfil.Enabled = true;        

    }

    protected void btnSalvarPerfil_Click(object sender, EventArgs e)
    {
        
        lblMsg.Visible = true;
        ClientScript.RegisterStartupScript(this.GetType(), "alert", "HideLabel();", true);       

             
        Usuario usuario = new Usuario();
        usuario.Nome = txtNome.Text;
        usuario.Login = txtLogin.Text;
        usuario.Codigo = Convert.ToInt32(Session["id"]);  //pega a id salva na session


        TipoUsuario tip = new TipoUsuario();
        tip.IdTipoUsuario = Convert.ToInt32(ddlPerfil.SelectedValue);
        usuario.FkTipoUsuario = tip;

        switch (UsuarioBD.Update(usuario))
        {
            case 0:
                //lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Atualizado com sucesso</div>";
                lblMsg.Text = "<div class='alert span4 alert-success'>Atualizado com sucesso</div>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>$('#myModal').modal('show');</script>", false);
               
                break;
            case -2:
                //lblMsg.Text = "</br></br> <div class='alert alert-danger'>Não pôde ser atualizado</div>";
                lblMsg.Text = "<div class='alert alert-danger'>Não pôde ser atualizado</div>";
                Page.ClientScript.RegisterStartupScript(this.GetType(), "script", "<script>$('#myModal').modal('show');</script>", false);
                break;
        }

        txtNome.Enabled = false;
        txtLogin.Enabled = false;
        ddlPerfil.Enabled = false;
    }
   
}