﻿<%@ Page Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="ConsultarClientes.aspx.cs" Inherits="pages_adm_ConsultarSensores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <div class="box box-info">

        <div class="box-header">
            <h3 class="box-title">Clientes</h3>
        </div>

        <div class="box-body">
            <div style="overflow-x:auto;">
            <asp:GridView ID="gridClientes" runat="server" CssClass="table table-responsive" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">

                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                <Columns>
                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/bower_components/Ionicons/png/512/search.png" ShowSelectButton="True">
                        <ControlStyle Height="16px" Width="16px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="cli_codigo" HeaderText="Código cliente" />
                    <asp:BoundField DataField="cli_tipo" HeaderText="Tipo pessoa" />
                    <asp:BoundField DataField="cli_nome" HeaderText="Nome" />
                    <asp:BoundField DataField="cli_email" HeaderText="Email" />
                    <asp:BoundField DataField="cli_data_vencimento" HeaderText="Data vencimento" />
                    <asp:BoundField DataField="nome_status" HeaderText="Status" />
                    <asp:BoundField DataField="pfi_cpf" HeaderText="CPF" />
                    <asp:BoundField DataField="cli_dt_cadastro" HeaderText="Data cadastro" />
                    <asp:BoundField DataField="pfi_data_nascimento" HeaderText="Data nascimento" />
                    <asp:BoundField DataField="pju_cnpj" HeaderText="CNPJ" />
                    <asp:BoundField DataField="pju_inscricao_estadual" HeaderText="Inscrição estadual" />
                    <asp:BoundField DataField="pju_razao_social" HeaderText="Razão social" />
                    <asp:BoundField DataField="tel_celular" HeaderText="Tel. Celular" />
                    <asp:BoundField DataField="tel_fixo" HeaderText="Tel. Fixo" />
                    <asp:BoundField DataField="tel_comercial" HeaderText="Tel. comercial" />
                    <asp:BoundField DataField="tel_recado" HeaderText="Tel. recado" />
                    <asp:BoundField DataField="end_logradouro" HeaderText="Logradouro" />
                    <asp:BoundField DataField="end_tipo_logradouro" HeaderText="Tipo logradouro" />
                    <asp:BoundField DataField="end_numero" HeaderText="Número" />
                    <asp:BoundField DataField="end_complemento" HeaderText="Complemento" />
                    <asp:BoundField DataField="end_bairro" HeaderText="Bairro" />
                    <asp:BoundField DataField="end_cidade" HeaderText="Cidade" />
                    <asp:BoundField DataField="end_estado" HeaderText="Estado" />
                    <asp:BoundField DataField="end_cep" HeaderText="CEP" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>

            <asp:Label ID="lbl" runat="server"></asp:Label>
            </div>
        </div>
    </div>

</asp:Content>
