﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="Perfil.aspx.cs" Inherits="pages_administrador_Perfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .espaco {
            margin-top: 10px;
        }
    </style>


    <div class="box box-info" />
        <div class="box-header with-border">
            <h3 class="box-title">Perfil</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">

            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtNome" runat="server" placeholder="Nome" Enabled="false" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label espaco">Login</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtLogin" runat="server" Enabled="false" CssClass="form-control" placeholder="Login"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label espaco">Senha</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtSenha" runat="server" Enabled="false" CssClass="form-control" placeholder="Senha"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Perfil</label>
                        <div class="col-sm-10">
                            <asp:DropDownList ID="ddlPerfil" runat="server" Enabled="false" CssClass="form-control">
                                <asp:ListItem Value="1">Administrador</asp:ListItem>
                                <asp:ListItem Value="2">Suporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box-footer espaco">
                <asp:Button ID="btnEditarPerfil" runat="server" Text="Editar" class="btn btn-default" OnClick="btnEditarPerfil_Click" />
                <asp:Button ID="btnSalvarPerfil" runat="server" Text="Salvar" class="btn btn-info pull-right" OnClick="btnSalvarPerfil_Click"/>

            </div>
            <div class="col-lg-12">
                <asp:Label ID="lblMsg" runat="server" Visible="false"></asp:Label>
            </div>

            


            <!-- /Privacidade-->
            <%-- <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Privacidade</h3>
                    <div class="box-body">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Senha atual</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtSenha" runat="server" Enabled="false" CssClass="form-control" placeholder="Senha"></asp:TextBox>
                            </div>

                            <label class="col-sm-2 control-label">Nova senha</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtNovaSenha" runat="server" Enabled="false" CssClass="form-control" placeholder="Nova senha"></asp:TextBox>
                            </div>

                            <label class="col-sm-2 control-label">Repita senha</label>
                            <div class="col-sm-10">
                                <asp:TextBox ID="txtRepitaSenha" runat="server" Enabled="false" CssClass="form-control" placeholder="Repita senha"></asp:TextBox>
                            </div>

                        </div>

                    </div>
                </div>
            </div>--%>

            <!-- /.box-body -->
            <%-- <div class="box-footer">

                <asp:Button ID="btnEditarPrivacidade" runat="server" Text="Editar" class="btn btn-default" OnClick="btnEditarPrivacidade_Click" />
                <asp:Button ID="btnSalvarPrivacidade" runat="server" Text="Salvar" class="btn btn-info pull-right" />
            </div>--%>
            <!-- /.box-footer -->
        </div>

         <script type="text/javascript">
             function HideLabel() {
                 var seconds = 5;
                 setTimeout(function () {
                     document.getElementById("<%=lblMsg.ClientID %>").style.display = "none";
             }, seconds * 1000);
             };
    </script>

</asp:Content>

