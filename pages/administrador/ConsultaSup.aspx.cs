﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_ConsultaSup : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CarregarGrid();
        }
    }

    private void CarregarGrid()
    {
        //DataSet ds = new DataSet();
        DataSet ds = UsuarioBD.SelectSuporte();
        int qtd = ds.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            gridUsuarios.DataSource = ds.Tables[0].DefaultView;
            gridUsuarios.DataBind();
            gridUsuarios.Visible = true;
            lbl.Text = "Foram encontrados " + qtd + " de registros";
        }
        else
        {
            gridUsuarios.Visible = false;
            lbl.Text = "Não foram encontrado registros...";
        }

    }

    protected void gridUsuarios_SelectedIndexChanged(object sender, EventArgs e)
    {
        Usuario usuario = new Usuario();
        usuario.Codigo = Convert.ToInt32(Session["id"]);  //pega a id salva na session


        switch (UsuarioBD.Delete(usuario))
        {
            case 0:
               // lblMsg.Text = "</br> </br><div class='alert span4 alert-success'>Atualizado com sucesso</div>";
                break;
            case -2:
                //lblMsg.Text = "</br></br> <div class='alert alert-danger'>Não pôde ser atualizado</div>";
                break;
        }


    }



}