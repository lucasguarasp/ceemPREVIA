﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_adm_ConsultarSensores : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CarregarGrid();
        }
    }

    private void CarregarGrid()
    {

        DataSet ds = EquipamentoDB.SelectAll();
        int qtd = ds.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            gridSensores.DataSource = ds.Tables[0].DefaultView;
            gridSensores.DataBind();
            gridSensores.Visible = true;
            lbl.Text = qtd + " Sensores";
        }
        else
        {
            gridSensores.Visible = false;
            lbl.Text = "Nenhum sensor cadastrado...";
        }
    }
}