﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="ConsultaSup.aspx.cs" Inherits="pages_administrador_ConsultaSup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-info">

        <div class="box-header">
            <h3 class="box-title">Suportes</h3>
        </div>

        <div class="box-body">

            <asp:GridView ID="gridUsuarios" runat="server" CssClass="table table-responsive" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnSelectedIndexChanged="gridUsuarios_SelectedIndexChanged">

                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />

                <Columns>
                    <asp:CommandField ButtonType="Image" SelectImageUrl="~/bower_components/Ionicons/png/512/search.png" ShowSelectButton="True">
                        <ControlStyle Height="16px" Width="16px" />
                    </asp:CommandField>
                    <asp:BoundField DataField="usu_nome" HeaderText="Nome" />
                    <asp:BoundField DataField="usu_login" HeaderText="Login" />
                    <asp:BoundField DataField="usu_status" HeaderText="Status" />
                    <asp:BoundField DataField="Date" HeaderText="Data de cadastro" />
                    <asp:BoundField DataField="tip_descricao" HeaderText="Descrição" />
                    <asp:CommandField ShowDeleteButton="True"></asp:CommandField>
                    <asp:CommandField ShowEditButton="True" />
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>

            <asp:Label ID="lbl" runat="server"></asp:Label>
        </div>
    </div>


</asp:Content>

