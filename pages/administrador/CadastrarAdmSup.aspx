﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="CadastrarAdmSup.aspx.cs" Inherits="pages_administrador_CadastrarAdmSup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .espaco {
            margin-top: 15px;
        }

        .direita {
            margin-right: 15px;
        }

        .baixo {
            margin-bottom: 40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Cadastro</h3>
        </div>


        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">


            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtNome" runat="server" class="form-control" placeholder="Nome"></asp:TextBox>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Login</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtLogin" runat="server" class="form-control" placeholder="Login"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Senha</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtSenha" runat="server" class="form-control" type="password" placeholder="Senha"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Repita Senha</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="txtRepitaSenha" runat="server" class="form-control" type="password" placeholder="Repita Senha"></asp:TextBox>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Perfil</label>
                        <div class="col-sm-8">
                            <asp:DropDownList ID="ddlTipo" runat="server" class="form-control">
                                <asp:ListItem Value="1">Administrador</asp:ListItem>
                                <asp:ListItem Value="2">Suporte</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <button type="button" class="fa fa-user-plus btn btn-defaultt" data-toggle="modal" data-target="#modal-cadastro-tipo"></button>
                    </div>
                </div>
                <%-- Modal cadastrar tipo de perfil --%>
                <div class="modal fade" id="modal-cadastro-tipo">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Cadastrar Perfil</h4>
                            </div>
                            <div class="modal-body">

                                <div class="form-group baixo">
                                    <label class="col-sm-4 control-label">Nome do perfil</label>
                                    <div class="col-sm-8">
                                        <asp:TextBox ID="txtPerfil" runat="server" class="form-control" type="text" placeholder="Nome do perfil"></asp:TextBox>
                                    </div>

                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Fechar</button>
                                <asp:Button ID="btnSalvar" runat="server" Text="Salvar" class="btn btn-primary" OnClick="btnSalvar_Click" />
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <%-- FIM Modal cadastrar tipo de perfil --%>

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-right espaco direita">

                        <asp:Button ID="btnLimpar" runat="server" Text="Limpar" class="btn btn-default" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" class="btn btn-info " OnClick="btnCadastrar_Click" />

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
                <div class="col-lg-12">
                    <asp:Label ID="lblMsg" runat="server" Visible="false" ></asp:Label>
                </div>
            </div>
        </div>

        <script type="text/javascript">
    function HideLabel() {
        var seconds = 3;
        setTimeout(function () {
            document.getElementById("<%=lblMsg.ClientID %>").style.display = "none";
        }, seconds * 1000);
    };
        </script>

    </div>
</asp:Content>

