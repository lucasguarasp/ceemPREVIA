﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/administrador/MasterPageAdm.master" AutoEventWireup="true" CodeFile="CadastrarPerfil.aspx.cs" Inherits="pages_administrador_CadastrarPerfil" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Cadastro Perfil</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">
            <div class="form-group">

                <label class=" col-sm-2 control-label">Nome</label>
                <div class="col-sm-10">
                    <asp:TextBox ID="txtNome" runat="server" class="form-control" placeholder="Nome"></asp:TextBox>

                </div>
            </div>
        </div>
    </div>

</asp:Content>

