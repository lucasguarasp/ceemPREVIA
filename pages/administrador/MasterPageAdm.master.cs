﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_administrador_MasterPageAdm : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Contador();
        }

        if (Session["nome"] == null || Session["perfil"] == null)
        {
            Response.Redirect("../../index/Login.aspx");
        }
        else
        {
            int perfil = Convert.ToInt32(Session["perfil"]);

            if (perfil == 1)
            {
               
                lblNome.Text = Session["nome"].ToString();
                lblData.Text = Session["data"].ToString();
            }
            else
            {
                Response.Redirect("../../index/Login.aspx");
            }
        }

        
    }

    private void Contador()
    {

        DataSet dsAdm = UsuarioBD.SelectAdm();
        DataSet dsSup = UsuarioBD.SelectSuporte();
        DataSet dsCli = UsuarioBD.SelectCliente(); // entidade
        DataSet dsPre = UsuarioBD.SelectPre();
        DataSet dsEqu = UsuarioBD.SelectEquipamentos();
        DataSet dsSen = UsuarioBD.SelectSensores();

        int qtd = dsAdm.Tables[0].Rows.Count;
        int qtd2 = dsSup.Tables[0].Rows.Count;
        int qtd3 = dsCli.Tables[0].Rows.Count;
        int qtd4 = dsPre.Tables[0].Rows.Count;
        int qtd5 = dsEqu.Tables[0].Rows.Count;
        int qtd6 = dsSen.Tables[0].Rows.Count;

        if (qtd > 0 || qtd2 >0 || qtd3 > 0 || qtd4 > 0 ||qtd5 > 0 || qtd6 >0)
        {
            lblAdmCont.Text = "" + qtd;
            lblSupCont.Text = "" + qtd2;
            lblCliCont.Text = "" + qtd3;
            lblPreCont.Text = "" + qtd4;
            lblEquCont.Text = "" + qtd5;
            lblSenCont.Text = "" + qtd5;
        }
        else
        {
            lblAdmCont.Text = "0";
            lblSupCont.Text = "0";
            lblCliCont.Text = "0";
            lblPreCont.Text = "0";
            lblEquCont.Text = "0";
            lblSenCont.Text = "0";
        }

    }


    protected void btnSair_Click(object sender, EventArgs e)
    {
        Session.Remove("nome"); Session.Remove("perfil");
        Response.Redirect("../../index/Login.aspx");
    }

}
