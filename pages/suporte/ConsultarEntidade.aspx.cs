﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_suporte_ConsultarEntidade : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            CarregarGrid();
        }
    }

    private void CarregarGrid()
    {

        DataSet ds = UsuarioBD.SelectCliente();
        int qtd = ds.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            gridUsuarios.DataSource = ds.Tables[0].DefaultView;
            gridUsuarios.DataBind();
            gridUsuarios.Visible = true;
            lbl.Text = "Foram encontrados " + qtd + " de registros";
        }
        else
        {
            gridUsuarios.Visible = false;
            lbl.Text = "Não foram encontrado registros...";
        }

    }

}