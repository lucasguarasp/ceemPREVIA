﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_suporte_MasterPageSuporte : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Contador();
        }

        if (Session["nome"] == null || Session["perfil"] == null)
        {
            Response.Redirect("../../index/Login.aspx");
        }
        else
        {
            int perfil = Convert.ToInt32(Session["perfil"]);
            if (perfil == 2)
            {
                lblSessao.Text = Session["nome"].ToString();
                lblData.Text = Session["data"].ToString();
            }
            else
            {
                Response.Redirect("../index/Login.aspx");
            }
        }

    }

    private void Contador()
    {

       
        DataSet ds3 = UsuarioBD.SelectCliente();
        //DataSet ds4 = UsuarioBD.SelectSuporte();
        DataSet ds5 = UsuarioBD.SelectPre();
        //DataSet ds6 = UsuarioBD.SelectSuporte();
        
       
        int qtd3 = ds3.Tables[0].Rows.Count;
        int qtd5 = ds5.Tables[0].Rows.Count;

        if (qtd3 > 0 || qtd5 >0)
        {

            lblCliCont.Text = "" + qtd3;
            lblPre.Text = "" + qtd5;
        }
        else
        {

            lblCliCont.Text = "0";
            lblPre.Text = "0";
        }

    }


    protected void btnSair_Click(object sender, EventArgs e)
    {
        Session.Remove("nome"); Session.Remove("perfil");
        Response.Redirect("../../index/Login.aspx");
    }
}
