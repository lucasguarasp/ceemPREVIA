﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_comum_MasterPageComum : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["nome"] == null || Session["perfil"] == null)
        {
            Response.Redirect("../../index/Login.aspx");
        }
        else
        {
            int perfil = Convert.ToInt32(Session["perfil"]);
            if (perfil == 4)
            {
                lblSessao.Text = Session["nome"].ToString();
                lblData.Text = Session["data"].ToString();
            }
            else
            {
                Response.Redirect("../index/Login.aspx");
            }
        }

    }

    protected void btnSair_Click(object sender, EventArgs e)
    {
        Session.Remove("nome"); Session.Remove("perfil");
        Response.Redirect("../../index/Login.aspx");
    }

}
