﻿<%@ Page Title="" Language="C#" MasterPageFile="~/pages/entidade/MasterPageEntidade.master" AutoEventWireup="true" CodeFile="CadastrarUsuario.aspx.cs" Inherits="pages_entidade_CadastrarUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .espaco {
            margin-top: 15px;
        }

        .direita {
            margin-right: 15px;
        }

        .baixo {
            margin-bottom: 40px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Cadastro</h3>
        </div>


        <!-- /.box-header -->
        <!-- form start -->

        <div class="box-body">


            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nome</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtNome" runat="server" class="form-control" placeholder="Nome"></asp:TextBox>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Login</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtLogin" runat="server" class="form-control" placeholder="Login"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Senha</label>
                        <div class="col-sm-10">
                            <asp:TextBox ID="txtSenha" runat="server" class="form-control" type="password" placeholder="Senha"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Repita Senha</label>
                        <div class="col-sm-9">
                            <asp:TextBox ID="txtRepitaSenha" runat="server" class="form-control" type="password" placeholder="Repita Senha"></asp:TextBox>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 espaco">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Perfil</label>
                        <div class="col-sm-8">
                            <asp:DropDownList ID="ddlTipo" runat="server" class="form-control">
                                <asp:ListItem Value="4">Usuario</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        
                    </div>
                </div>
                

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 text-right espaco direita">

                        <asp:Button ID="btnLimpar" runat="server" Text="Limpar" class="btn btn-default" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnCadastrar" runat="server" Text="Cadastrar" class="btn btn-info " OnClick="btnCadastrar_Click" />

                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </div>
                </div>
                <div class="col-lg-12">
                    <asp:Label ID="lblMsg" runat="server" Visible="false" ></asp:Label>
                </div>
            </div>
        </div>

        <script type="text/javascript">
    function HideLabel() {
        var seconds = 3;
        setTimeout(function () {
            document.getElementById("<%=lblMsg.ClientID %>").style.display = "none";
        }, seconds * 1000);
    };
        </script>

    </div>



</asp:Content>

