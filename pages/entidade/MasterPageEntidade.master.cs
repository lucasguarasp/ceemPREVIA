﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pages_entidade_MasterPageEntidade : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            Contador();
        }


        if (Session["nome"] == null || Session["perfil"] == null)
        {
            Response.Redirect("../../index/Login.aspx");
        }
        else
        {
            int perfil = Convert.ToInt32(Session["perfil"]);
            if (perfil == 3)
            {
                lblNome.Text = Session["nome"].ToString();
                lblData.Text = Session["data"].ToString();
            }
            else
            {
                Response.Redirect("../index/Login.aspx");
            }
        }

    }

    private void Contador()
    {

        DataSet dsUsu = UsuarioBD.SelectUsuComum();

        int qtd = dsUsu.Tables[0].Rows.Count;

        if (qtd > 0)
        {
            lblUsuCont.Text = "" + qtd;
        }
        else
        {
            lblUsuCont.Text = "0";
        }

    }

    protected void btnSair_Click(object sender, EventArgs e)
    {
        Session.Remove("nome"); Session.Remove("perfil");
        Response.Redirect("../../index/Login.aspx");
    }
}
