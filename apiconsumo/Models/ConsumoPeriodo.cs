﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace apiconsumo.Models
{
    public class ConsumoPeriodo
    {
        private ConsumoContext context;

        public string Periodo { get; set; }

        public double Potencia { get; set; }

        public double KWh { get; set; }

        public int Sensor { get; set; }

        public int Equipamento { get; set; }

        public string LocalSensor { get; set; }
        
        public string LocalEquipamento { get; set; }
    }
}
