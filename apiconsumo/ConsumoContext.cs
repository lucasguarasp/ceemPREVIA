﻿using System;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using apiconsumo.Models;
using System.Text;

namespace apiconsumo
{
    public class ConsumoContext
    {
        private Dictionary<string, object> parametros;

        public string ConnectionString { get; set; }

        public ConsumoContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            MySqlConnection conn = new MySqlConnection(ConnectionString);
            conn.Open();
            return conn;
        }

        public void Insert(Dictionary<string, object> args)
        {
            StringBuilder insert = new StringBuilder();
            insert.AppendLine("INSERT INTO log_log_consumo(log_instante, log_potencia, sen_id"); //, hba_codigo)");
            insert.AppendLine("VALUES(@instante, @potencia, @sen_id)");//, @hba_codigo)");

            using (MySqlConnection conn = GetConnection())
            {
                MySqlCommand cmd = new MySqlCommand(insert.ToString(), conn);
                cmd.Parameters.AddWithValue("@instante", (DateTime) args["instante"]);
                cmd.Parameters.AddWithValue("@potencia", (double) args["potencia"]);
                cmd.Parameters.AddWithValue("@sen_id", (int) args["sensor"]);
                //cmd.Parameters.AddWithValue("@hba_codigo", (int) args["bandeira"]);
                cmd.ExecuteNonQuery();
            }
        }

        private StringBuilder Select()
        {
            StringBuilder select = new StringBuilder().AppendLine("SELECT");
            switch (parametros["tipo"])
            {
                case "mes":
                select.AppendLine("TO_NOME_MES(log_instante) periodo,"); break;
                case "dia_semana":
                select.AppendLine("TO_DIA_SEMANA(log_instante) periodo,"); break;
                case "dia_mes":
                select.AppendLine("DAYOFMONTH(log_instante) periodo,"); break;
                case "ano":
                select.AppendLine("YEAR(log_instante) periodo,"); break;
                default:
                /*Nunca deve alcançar o default*/
                throw new FormatException("Código está errado!!");
            }
            select.AppendLine("SUM(log_potencia) potencia,")
            .AppendLine("(SUM(log_potencia)/1000) * ")
                .Append("(TIMESTAMPDIFF(SECOND, @inicio, @fim)/3600) kwh,")
            .AppendLine("sen.sen_id sensor,")
            .AppendLine("equ.equ_id equipamento,")
            .AppendLine("loc_sen.loc_nome local_sensor,")
            .AppendLine("loc_equ.loc_nome local_equipamento");
            return select;
        }

        private StringBuilder From()
        {
            StringBuilder from = new StringBuilder();
            from.AppendLine("FROM log_log_consumo cons")
            .AppendLine("INNER JOIN sen_sensor sen ON sen.sen_id = cons.sen_id")
            .AppendLine("INNER JOIN loc_local_instalacao loc_sen ON loc_sen.loc_codigo = sen.loc_codigo")
            .AppendLine("INNER JOIN equ_equipamento equ ON equ.equ_id = sen.equ_id")
            .AppendLine("INNER JOIN loc_local_instalacao loc_equ ON loc_equ.loc_codigo = equ.loc_codigo");
            return from;
        }

        private StringBuilder Where()
        {
            StringBuilder where = new StringBuilder();

            where.AppendLine("WHERE equ.cli_codigo = @cliente");
            
            if (parametros.ContainsKey("equipamentos"))
            {
                where.AppendLine("AND equ.equ_id IN (");

                int nEquipamentos = ((string) parametros["equipamentos"]).Split(",").Length;

                where.Append("@equipamento_0");
                for (int equ = 1; equ < nEquipamentos; equ++)
                {
                    where.Append(String.Format(",@equipamento_{0}", equ));
                }
                where.Append(")");
            }
                

            if (parametros.ContainsKey("sensores"))
            {
                where.AppendLine("AND sen.sen_id IN (");

                int nEquipamentos = ((string) parametros["sensores"]).Split(",").Length;

                where.Append("@sensor_0");
                for (int equ = 1; equ < nEquipamentos; equ++)
                {
                    where.Append(",@sensor_" + equ);
                }
                where.Append(")");
            }

            where.AppendLine("AND log_instante BETWEEN @inicio AND @fim");
            return where;
        }

        private StringBuilder GroupBy()
        {
            StringBuilder groupBy = new StringBuilder();
            groupBy.AppendLine("GROUP BY sen.sen_id,");
            switch (parametros["tipo"])
            {
                case "mes":
                groupBy.AppendLine("TO_NOME_MES(log_instante)"); break;
                case "dia_semana":
                groupBy.AppendLine("TO_DIA_SEMANA(log_instante)"); break;
                case "dia_mes":
                groupBy.AppendLine("DAYOFMONTH(log_instante)"); break;
                case "ano":
                groupBy.AppendLine("YEAR(log_instante)"); break;
                default:
                /*Nunca deve alcançar o default*/
                throw new FormatException("Código está errado!!");
            }
            return groupBy;
        }

        private StringBuilder OrderBy()
        {
            StringBuilder orderBy = new StringBuilder();
            orderBy.AppendLine("ORDER BY")
            .AppendLine("equ.equ_id,")
            .AppendLine("sen.sen_id");
            return orderBy.Append(";");
        }

        private string GetQuery()
        {
            return Select().Append(From())
                    .Append(Where())
                    .Append(GroupBy())
                    .Append(OrderBy()).ToString();
        }

        public List<ConsumoPeriodo> GetConsumo(Dictionary<String, object> args)
        {
            parametros = args;

            List<ConsumoPeriodo> list = new List<ConsumoPeriodo>();

            using (MySqlConnection conn = GetConnection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(GetQuery(), conn);


                cmd.Parameters.AddWithValue("@cliente",
                    Convert.ToInt32(parametros["cliente"]));

                cmd.Parameters.AddWithValue("@inicio",
                    Convert.ToDateTime(parametros["inicio"]));

                cmd.Parameters.AddWithValue("@fim",
                    Convert.ToDateTime(parametros["fim"]));
                try
                {
                    if (parametros.ContainsKey("equipamentos"))
                    {
                        string[] equipamentos = ((string) parametros["equipamentos"]).Split(",");

                        for (int equ = 0; equ < equipamentos.Length; equ++)
                        {
                            cmd.Parameters.AddWithValue("@equipamento_" + equ,
                                Convert.ToInt32(equipamentos[equ]));

                        }
                    }

                    if (parametros.ContainsKey("sensores"))
                    {
                        string[] sensores = ((string) parametros["sensores"]).Split(",");

                        for (int sen = 0; sen < sensores.Length; sen++)
                        {
                            cmd.Parameters.AddWithValue("@sensor_" + sen,
                                Convert.ToInt32(sensores[sen]));
                        }
                    }
                }
                catch (FormatException)
                {
                    return list;
                }

                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new ConsumoPeriodo() {
                            Periodo = reader["periodo"].ToString(),
                            Potencia = Convert.ToDouble(reader["potencia"]),
                            KWh = Convert.ToDouble(reader["kwh"]),
                            Sensor = Convert.ToInt32(reader["sensor"]),
                            Equipamento = Convert.ToInt32(reader["equipamento"]),
                            LocalSensor = reader["local_sensor"].ToString(),
                            LocalEquipamento = reader["local_equipamento"].ToString()
                        });
                    }
                }
            }
            return list;
        }
    }
}
