﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Net.Http;

using apiconsumo.Models;

namespace apiconsumo.Controllers
{
     [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        //[HttpGet("{tipo}/cliente{cliente}/periodo{inicio};{fim}/{equ=-1}/{sen=-1}")]
        [HttpGet("{tipo}")]
        public IEnumerable<ConsumoPeriodo> Get(
            string tipo, int cliente, DateTime inicio, DateTime fim, string equipamentos="", string sensores="")
        {
            ConsumoContext context = 
                HttpContext.RequestServices.GetService(
                    typeof(ConsumoContext)) as ConsumoContext;

            if (context == null)
                return new List<ConsumoPeriodo>();

            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("tipo", tipo);
            args.Add("cliente", cliente);
            args.Add("inicio", inicio);
            args.Add("fim", fim);

            if (equipamentos != "")
                args.Add("equipamentos", equipamentos);

            if (sensores != "")
                args.Add("sensores", sensores);

            return context.GetConsumo(args);
        }

        // POST api/values
        [HttpPost]
        public void Post(
            DateTime instante,
            double potencia,
            int sensor,
            int bandeira)
        {
            ConsumoContext context =
                HttpContext.RequestServices.GetService(
                    typeof(ConsumoContext)) as ConsumoContext;
            
            if (context == null) return;
      
            Dictionary<string, object> args = new Dictionary<string, object>();
            args.Add("instante", instante);
            args.Add("potencia", potencia);
            args.Add("sensor", sensor);
            // args.Add("bandeira", bandeira);

            context.Insert(args);
        }
    }
}
