show tables;
show databases;

use ceem;

# FUNCOES

# Funcao que retorna o dia do mes recebendo uma data
# EXEMPLO - SELECT TO_NOME_MES(STR_TO_DATE('15/05/2012 8:06:26 AM', '%d/%m/%Y %r')) AS mes;
DROP FUNCTION IF EXISTS TO_NOME_MES;
CREATE FUNCTION TO_NOME_MES(param_data DATETIME)
RETURNS VARCHAR(9)
RETURN ELT(MONTH(param_data),
		'Janeiro', 'Fevereiro', 'Março', 'Abril',
		'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro',
		'Outubro', 'Novembro', 'Dezembro');

#SELECT FORMATO_DATA_PADRAO(NOW());        
DROP FUNCTION IF EXISTS FORMATO_DATA_PADRAO;
CREATE FUNCTION FORMATO_DATA_PADRAO(param_data DATETIME)
RETURNS VARCHAR(10)
RETURN DATE_FORMAT(param_data, '%d/%m/%Y');


# Funcao que retorna o dia da semana recebendo uma data
# EXEMPLO - SELECT TO_DIA_SEMANA(STR_TO_DATE('15/05/2012 8:06:26 AM', '%d/%m/%Y %r')) AS dia_semana;
DROP FUNCTION IF EXISTS TO_DIA_SEMANA;
CREATE FUNCTION TO_DIA_SEMANA(param_data DATETIME)
RETURNS VARCHAR(9)
RETURN ELT(DAYOFWEEK(param_data),
		'Domingo', 'Segunda', 'Terça', 'Quarta',
		'Quinta', 'Sexta', 'Sábado');

# Retorna os nomes dos status do cliente
# EXEMPLO - SELECT TO_NOME_STATUS_CLIENTE('P');
DROP FUNCTION IF EXISTS TO_NOME_STATUS_CLIENTE;
CREATE FUNCTION TO_NOME_STATUS_CLIENTE(cli_status VARCHAR(1))
RETURNS VARCHAR(50)
	RETURN CASE WHEN cli_status = 'A' THEN 'Ativo'
		   ELSE CASE WHEN cli_status = 'B' THEN 'Bloqueado'
           ELSE CASE WHEN cli_status = 'D' THEN 'Desativado'
           ELSE CASE WHEN cli_status = 'P' THEN 'Pré cadastro'
	END END END END;

# INSERTS 

#Insert Perfil de usuário
INSERT INTO tip_tipo_usuario(tip_descricao)
VALUES ("Administrador"), ("Suporte"), ("Entidade"), ("Usuário");

INSERT INTO `ceem`.`cli_cliente`
(`cli_tipo`, `cli_nome`, `cli_email`, `cli_data_vencimento`, `cli_status`, `cli_dt_cadastro`)
VALUES ('J', 'COCA-COLA', 'm@m.m', NOW(), 'D', NOW());

INSERT INTO `ceem`.`cli_cliente`
(`cli_tipo`, `cli_nome`, `cli_email`, `cli_data_vencimento`, `cli_status`, `cli_dt_cadastro`)
VALUES ('F', 'Maicon M.', 'm@m.m', NOW(), 'P', NOW());


#Insert de Nome, Login, Status, Data e Perfil de usuário
INSERT INTO `ceem`.`usu_usuario`
(`usu_nome`,`usu_login`,`usu_senha`,`usu_status`, `cli_codigo_entidade`, `usu_dt_cadastro`,`tip_tipo_usuario_tip_codigo`)
VALUES ("coca", "coca1", "123", 1, 1, now(), 3);

INSERT INTO `ceem`.`loc_local_instalacao`
(`loc_nome`, `loc_descricao`, `loc_dt_instalacao`, `cli_codigo`)
VALUES ('DEPARTAMENTO DE FINANÇAS', '', NOW(), 1);

INSERT INTO `ceem`.`equ_equipamento`
(`equ_dt_cadastro`, `cli_codigo`, `loc_codigo`)
VALUES (NOW(), 1, 1);

INSERT INTO `ceem`.`sen_sensor`
(`sen_dt_cadastro`, `equ_id`, `loc_codigo`)
VALUES (NOW(), 1, 1);

INSERT INTO `ceem`.`ban_bandeira_tar`
(`ban_nome`)
VALUES ('VERMELHA');

INSERT INTO `ceem`.`ban_bandeira_tar`
(`ban_nome`, `cli_codigo`)
VALUES ('VERMELHA', 1);

INSERT INTO `ceem`.`hba_historico_bandeira`
(`hba_valor`, `hba_limite_kw`, `hba_dt_cadastro`, `hba_is_ativa`, `ban_codigo`)
VALUES
(2, 100, NOW(), true, 1);

INSERT INTO `ceem`.`dat_data_band`
(`dat_inicio`, `dat_fim`, `dat_verao`, `hba_codigo`)
VALUES (DATE(STR_TO_DATE('15/05/2012 8:06:26 AM', '%d/%m/%Y %r')), NOW(), false, 1);

INSERT INTO `ceem`.`log_log_consumo`
(`log_instante`, `log_potencia`, `sen_id`, `hba_codigo`)
VALUES (NOW(), 55.0, 1, 1);

INSERT INTO pju_pessoa_juridica
(pju_cnpj, pju_inscricao_estadual, pju_razao_social, pju_nome_fantasia, cli_codigo)
VALUES('123213123', '213213/21321', 'COCA TM', 'COCA-COLA', 1);

INSERT INTO pfi_pessoa_fisica
(pfi_cpf, pfi_data_nascimento, cli_codigo)
VALUES('12321312', NOW(), 2);

INSERT INTO tel_telefone
(tel_celular, tel_fixo, tel_comercial, tel_recado, tel_dt_mudanca_tel, cli_codigo)
VALUES('21321-32132', '21321-32132', '21321-32132', '21321-32132', NOW(), 2);

INSERT INTO end_endereco
(end_logradouro, end_tipo_logradouro, end_numero, end_complemento,
end_bairro, end_cidade, end_estado, end_cep, end_dt_mudanca_end, cli_codigo)
 VALUES('RUA BLAAA', 'RUA', '213123', '', 'asdiasdas', 'APARECIDA', 'SP', '128392-21', NOW(), 2);

INSERT INTO `cli_cliente` VALUES (1,'J','COCA-COLA','m@m.m','2017-12-03','D','2017-12-03'),(2,'F','Por que não atualiza?','','2017-12-06','P','2017-12-06'),(19,'F','Maicon','Maicon@m.com','0001-01-01','P','2017-12-06'),(20,'F','Maicon','Maicon@m.com','0001-01-01','P','2017-12-06'),(33,'F','Maicon','Maicon@m.com','0001-01-01','P','2017-12-06'),(34,'F','Maicon','Maicon@m.com','0001-01-01','P','2017-12-06'),(39,'F','Maicon','Maicon@m.com','0001-01-01','P','2017-12-06'),(40,'F','Maicon :( :C :) :D','Maiconandsilva@m.com','0001-01-01','P','2017-12-06');
INSERT INTO `ban_bandeira_tar` VALUES (1,'VERMELHA',NULL),(2,'VERMELHA',1);
INSERT INTO `hba_historico_bandeira` VALUES (1,2,'100','2017-12-04',1,1);
INSERT INTO `dat_data_band` VALUES (2,'2012-05-15','2017-12-04',0,1),(3,'2012-05-15','2017-12-04',0,1),(4,'2012-05-15','2017-12-04',0,1),(5,'2012-05-15','2017-12-04',0,1),(6,'2012-05-15','2017-12-04',0,1),(7,'2012-05-15','2017-12-04',0,1),(8,'2012-05-15','2017-12-04',0,1),(9,'2012-05-15','2017-12-04',0,1),(10,'2012-05-15','2017-12-04',0,1),(11,'2012-05-15','2017-12-04',0,1);
INSERT INTO `end_endereco` VALUES (1,'RUA BLAAA','RUA','213123','','asdiasdas','APARECIDA','SP','128392-21','2017-12-06',1),(2,'MAICON','RUA','213123','','asdiasdas','APARECIDA','SP','128392-21','2017-12-06',2),(5,'Zezé Valadão','Avenida','1232','','Sei la','APARECIDA','SP','21312-231','2017-12-06',34),(6,'Zezé Valadão','Avenida lala','1232','','Sei la','APARECIDA CITY','SP','21312-231','2017-12-07',40);
INSERT INTO `loc_local_instalacao` VALUES (1,'DEPARTAMENTO DE FINANÇAS','','2017-12-03',1);
INSERT INTO `equ_equipamento` VALUES (1,'2017-12-03',1,1),(2,'2017-12-04',1,1);
INSERT INTO `sen_sensor` VALUES (1,'2017-12-03',1,1),(2,'2017-12-04',1,1);
INSERT INTO `log_log_consumo` VALUES (1,'2017-12-04 00:00:00',36.6,2,1),(2,'2017-12-04 00:00:00',36.6,2,1),(3,'2017-12-04 00:00:00',36.6,2,1),(4,'2017-12-04 00:00:00',36.6,2,1),(5,'2017-12-04 00:00:00',36.6,2,1),(6,'2017-12-04 00:00:00',36.6,2,1),(7,'2017-12-04 00:00:00',16.6,2,1),(8,'2017-12-04 00:00:00',16.6,2,1),(9,'2017-12-04 00:00:00',16.6,2,1),(10,'2017-12-04 00:00:00',16.6,2,1),(11,'2017-12-04 00:00:00',16.6,2,1),(12,'2017-12-04 00:00:00',16.6,2,1),(13,'2017-12-04 00:00:00',16.6,2,1),(14,'2017-12-04 00:00:00',16.6,2,1),(15,'2017-12-04 00:00:00',16.6,2,1),(16,'2017-12-04 00:00:00',16.6,2,1),(17,'2017-12-04 00:00:00',16.6,2,1),(18,'2017-12-04 00:00:00',16.6,2,1),(19,'2017-12-04 00:00:00',16.6,2,1),(20,'2017-12-04 00:00:00',16.6,2,1),(21,'2017-12-04 00:00:00',16.6,2,1),(22,'2017-12-04 00:00:00',16.6,2,1),(23,'2017-12-04 00:00:00',16.6,2,1),(24,'2017-12-04 00:00:00',16.6,2,1),(25,'2017-12-04 00:00:00',16.6,2,1),(26,'2017-12-04 00:00:00',16.6,2,1),(27,'2017-12-04 00:00:00',16.6,2,1),(28,'2017-12-04 00:00:00',16.6,2,1),(29,'2017-12-04 00:00:00',16.6,2,1),(30,'2017-12-04 00:00:00',16.6,2,1),(31,'2017-12-04 00:00:00',16.6,2,1),(32,'2017-12-04 00:00:00',16.6,2,1),(33,'2017-12-04 00:00:00',16.6,2,1),(34,'2017-12-04 00:00:00',16.6,2,1),(35,'2017-12-04 00:00:00',16.6,2,1),(36,'2017-12-04 00:00:00',16.6,2,1),(37,'2017-12-04 00:00:00',16.6,2,1),(38,'2017-12-04 00:00:00',16.6,2,1),(39,'2017-12-04 00:00:00',55,2,1),(40,'2017-12-04 00:00:00',55,2,1),(41,'2017-12-04 00:00:00',55,2,1),(42,'2017-12-04 00:00:00',55,2,1),(43,'2017-12-04 00:00:00',55,2,1),(44,'2017-12-04 00:00:00',55,2,1),(45,'2017-12-04 00:00:00',55,2,1),(46,'2017-12-04 00:00:00',55,2,1),(47,'2017-12-04 00:00:00',55,2,1),(48,'2017-12-04 00:00:00',55,2,1),(49,'2017-12-04 00:00:00',55,2,1),(50,'2017-12-04 00:00:00',55,1,1),(51,'2017-12-04 00:00:00',55,1,1),(52,'2017-12-04 00:00:00',55,1,1),(53,'2017-12-04 00:00:00',55,1,1),(54,'2017-12-04 00:00:00',55,1,1),(55,'2017-12-04 00:00:00',55,1,1),(56,'2017-12-04 00:00:00',55,1,1),(57,'2017-12-04 00:00:00',55,1,1),(58,'2017-12-04 00:00:00',55,1,1),(59,'2017-12-04 00:00:00',55,1,1),(60,'2017-12-04 00:00:00',55,1,1),(61,'2017-12-04 00:00:00',55,1,1),(62,'2017-12-04 00:00:00',55,1,1),(63,'2017-12-04 00:00:00',55,1,1),(64,'2017-12-04 00:00:00',55,1,1),(65,'2017-12-04 00:00:00',55,1,1),(66,'2017-12-04 00:00:00',55,1,1),(67,'2017-12-05 01:00:31',55,1,1),(68,'2017-12-05 01:00:34',55,1,1),(69,'2017-12-05 01:00:35',55,1,1),(70,'2017-12-05 01:00:37',55,1,1),(71,'2017-12-05 02:00:37',55.555555555,2,1),(72,'2017-12-06 02:00:37',66666666.666666,1,1),(73,'2017-12-06 02:00:37',66666666.666666,1,1),(74,'2017-12-06 03:00:37',66666666.666666,1,1),(75,'2017-12-06 04:00:37',66666666.666666,1,1),(76,'2017-12-07 04:00:37',66666666.666666,1,1);
INSERT INTO `pfi_pessoa_fisica` VALUES (1,'12321312','2017-12-06',2),(2,'1212','2017-12-06',1),(8,'12321312312/X','2017-12-06',34),(10,'12321111/X','2017-12-07',40);
INSERT INTO `pju_pessoa_juridica` VALUES (1,'123213123','213213/21321','COCA TM','COCA-COLA',1);
INSERT INTO `tel_telefone` VALUES (1,'21321-32132','21321-32132','21321-32132','21321-32132','2017-12-06',1),(2,'21321-32132','21321-32132','21321-32132','21321-32132','2017-12-06',2),(6,'','','',NULL,'2017-12-06',34),(7,'','','','','2017-12-07',40);
INSERT INTO `tip_tipo_usuario` VALUES (1,'Administrador'),(3,'Entidade'),(2,'Suporte'),(4,'Usuário');
INSERT INTO `usu_usuario`
(usu_codigo, usu_nome, usu_login, usu_senha, usu_status, usu_dt_cadastro, tip_tipo_usuario_tip_codigo)
 VALUES (1,'coca','coca1','123',1,'2017-12-03',3);


COMMIT;
# SELECTS

#Select de Nome, Login, Status, Data e Perfil de usuário
SELECT usu_nome, usu_login, TO_NOME_STATUS_CLIENTE(usu_status) usu_status, 
(Date_Format(usu_dt_cadastro,'%d/%m/%Y')) data_cadastro
FROM usu_usuario
	INNER JOIN tip_tipo_usuario ON tip_codigo = tip_tipo_usuario_tip_codigo
WHERE tip_tipo_usuario_tip_codigo = 1;

ALTER TABLE `usu_usuario` CHANGE COLUMN `tip_codigo` `tip_tipo_usuario_tip_codigo` VARCHAR(255);
#Select Perfil de usuário
SELECT  tip_descricao FROM tip_tipo_usuario;

#Dia da semana
SELECT
	TO_DIA_SEMANA(log_instante) periodo,
	SUM(log_potencia) potencia,
	(SUM(log_potencia)/1000) * 
	(TIMESTAMPDIFF(SECOND, @inicio, @fim)/3600) kwh,
    loc_sen.loc_nome local_sensor,
	loc_equ.loc_nome local_equipamento
FROM log_log_consumo cons
	INNER JOIN sen_sensor sen ON sen.sen_id = cons.sen_id
	INNER JOIN loc_local_instalacao loc_sen ON loc_sen.loc_codigo = sen.loc_codigo
	INNER JOIN equ_equipamento equ ON equ.equ_id = sen.equ_id
	INNER JOIN loc_local_instalacao loc_equ ON loc_equ.loc_codigo = equ.loc_codigo
WHERE equ.cli_codigo = @cliente
	AND log_instante BETWEEN @inicio AND @fim
	#AND equ.equ_id = @equipamento
	#AND sen.sen_id = @sensor
GROUP BY sen.sen_id, TO_DIA_SEMANA(log_instante)
ORDER BY equ.equ_id, sen.sen_id;

#Dia do mes
SELECT
	DAYOFMONTH(log_instante) periodo,
	SUM(log_potencia) potencia,
	(SUM(log_potencia)/1000) * 
	(TIMESTAMPDIFF(SECOND, STR_TO_DATE('12/04/2017 8:06:26 AM', '%d/%m/%Y %r'), NOW())/3600) kwh,
	sen.sen_id,
    equ.equ_id,
    loc_sen.loc_nome local_sensor,
	loc_equ.loc_nome local_equipamento
FROM log_log_consumo cons
	INNER JOIN sen_sensor sen ON sen.sen_id = cons.sen_id
	INNER JOIN loc_local_instalacao loc_sen ON loc_sen.loc_codigo = sen.loc_codigo
	INNER JOIN equ_equipamento equ ON equ.equ_id = sen.equ_id
	INNER JOIN loc_local_instalacao loc_equ ON loc_equ.loc_codigo = equ.loc_codigo
WHERE equ.cli_codigo = 1
	AND log_instante BETWEEN STR_TO_DATE('12/04/2017 8:06:26 AM', '%d/%m/%Y %r') AND NOW()
	AND equ.equ_id IN (1)
	AND sen.sen_id IN (1, 2)
GROUP BY sen.sen_id, DAYOFMONTH(log_instante)
ORDER BY equ.equ_id, sen.sen_id;

# Consumo por mes
SELECT
	TO_NOME_MES(log_instante) periodo,
	SUM(log_potencia) potencia,
	(SUM(log_potencia)/1000) * 
	(TIMESTAMPDIFF(SECOND, STR_TO_DATE('12/04/2017 8:06:26 AM', '%d/%m/%Y %r'), NOW())/3600) kwh,
    loc_sen.loc_nome local_sensor,
	loc_equ.loc_nome local_equipamento,
    sen.sen_id,
    equ.equ_id
FROM log_log_consumo cons
	INNER JOIN sen_sensor sen ON sen.sen_id = cons.sen_id
	INNER JOIN loc_local_instalacao loc_sen ON loc_sen.loc_codigo = sen.loc_codigo
	INNER JOIN equ_equipamento equ ON equ.equ_id = sen.equ_id
	INNER JOIN loc_local_instalacao loc_equ ON loc_equ.loc_codigo = equ.loc_codigo
WHERE equ.cli_codigo = 1
	AND log_instante BETWEEN STR_TO_DATE('12/04/2017 8:06:26 AM', '%d/%m/%Y %r') AND NOW()
	#AND equ.equ_id = @equipamento
	#AND sen.sen_id = @sensor
GROUP BY sen.sen_id, TO_NOME_MES(log_instante)
ORDER BY equ.equ_id, sen.sen_id;

#Consumo por ano
SELECT
	YEAR(log_instante) periodo,
	SUM(log_potencia) potencia,
	(SUM(log_potencia)/1000) * 
	(TIMESTAMPDIFF(SECOND, @inicio, @fim)/3600) kwh,
    loc_sen.loc_nome local_sensor,
	loc_equ.loc_nome local_equipamento
FROM log_log_consumo cons
	INNER JOIN sen_sensor sen ON sen.sen_id = cons.sen_id
	INNER JOIN loc_local_instalacao loc_sen ON loc_sen.loc_codigo = sen.loc_codigo
	INNER JOIN equ_equipamento equ ON equ.equ_id = sen.equ_id
	INNER JOIN loc_local_instalacao loc_equ ON loc_equ.loc_codigo = equ.loc_codigo
WHERE equ.cli_codigo = @cliente 
	AND log_instante BETWEEN @inicio AND @fim
	#AND equ.equ_id = @equipamento
	#AND sen.sen_id = @sensor
GROUP BY sen.sen_id, YEAR(log_instante)
ORDER BY equ.equ_id, sen.sen_id;

SELECT
-- CLIENTE
	cli.cli_codigo, cli_tipo, cli_nome, cli_email,
	FORMATO_DATA_PADRAO(cli_data_vencimento) cli_data_vencimento,
	cli_status, TO_NOME_STATUS_CLIENTE(cli_status) nome_status,
	-- PESSOA FISICA
	pfi_cpf,
	FORMATO_DATA_PADRAO(cli_dt_cadastro) cli_dt_cadastro, 
	FORMATO_DATA_PADRAO(pfi_data_nascimento) pfi_data_nascimento,
	-- PESSOA JURIDICA
	pju_cnpj, pju_inscricao_estadual, pju_razao_social, pju_nome_fantasia,
	-- TELEFONE
	tel_celular, tel_fixo, tel_comercial, tel_recado,
	FORMATO_DATA_PADRAO(tel_dt_mudanca_tel) tel_dt_mudanca_tel,
	-- ENDERECO
	end_logradouro, end_tipo_logradouro, end_numero, end_complemento, 
	end_bairro, end_cidade, end_estado, end_cep,
	FORMATO_DATA_PADRAO(end_dt_mudanca_end) tel_dt_mudanca_tel
FROM cli_cliente cli
	INNER JOIN end_endereco end ON end.cli_codigo = cli.cli_codigo
	INNER JOIN tel_telefone tel ON tel.cli_codigo = cli.cli_codigo
	LEFT JOIN pju_pessoa_juridica pju ON pju.cli_codigo = cli.cli_codigo
	LEFT JOIN pfi_pessoa_fisica pfi ON pfi.cli_codigo = cli.cli_codigo
WHERE cli.cli_codigo IN(1, 40)
ORDER BY cli.cli_codigo DESC;

SELECT
	sen.sen_id,
    loc_sen.loc_nome local_sensor,
	loc_sen.loc_descricao loc_descricao_sensor,
    loc_sen.loc_dt_instalacao loc_dt_instalacao_sensor,
    equ.equ_id,
	loc_equ.loc_nome loc_equipamento,
    loc_equ.loc_descricao loc_descricao_equipamento,
    loc_equ.loc_dt_instalacao loc_dt_instalacao_equipamento,
    loc_equ.cli_codigo
FROM sen_sensor sen
	INNER JOIN loc_local_instalacao loc_sen ON loc_sen.loc_codigo = sen.loc_codigo
	INNER JOIN equ_equipamento equ ON equ.equ_id = sen.equ_id
	INNER JOIN loc_local_instalacao loc_equ ON loc_equ.loc_codigo = equ.loc_codigo
WHERE equ.cli_codigo IN (1, 2)#(@cliente)
	AND equ.equ_id IN (1) #(@equipamento)
	AND sen.sen_id IN (1,2) #(@sensor)
ORDER BY equ.equ_id, sen.sen_id;

UPDATE usu_usuario SET cli_codigo_usuario = 1 WHERE usu_codigo = 1;

# SELECT ALL STATEMENTS
SELECT * FROM ban_bandeira_tar;
SELECT * FROM cli_cliente;
SELECT * FROM dat_data_band;
SELECT * FROM ema_email;
SELECT * FROM end_endereco;
SELECT * FROM equ_equipamento;
SELECT * FROM hba_historico_bandeira;
SELECT * FROM loc_local_instalacao;
SELECT * FROM log_log_consumo;
SELECT * FROM mod_modulo;
SELECT * FROM pag_tipo_pagamento;
SELECT * FROM pfi_pessoa_fisica;
SELECT * FROM pju_pessoa_juridica;
SELECT * FROM pxm_tipo_modulo;
SELECT * FROM sen_sensor;
SELECT * FROM tel_telefone;
SELECT * FROM tip_tipo_usuario;
SELECT * FROM usu_usuario;
SELECT * FROM uxe_usuario_email;
SELECT * FROM uxm_usuario_modulo;
