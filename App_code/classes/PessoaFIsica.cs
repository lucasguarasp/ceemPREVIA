﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de PessoaFisica
/// </summary>
public class PessoaFisica : Cliente
{
    private int pfi_codigo;
    private string pfi_cpf;
    private DateTime pfi_data_nascimento;

    public PessoaFisica()
    {
        Cli_tipo = "F";
    }

    public int Pfi_codigo
    {
        get
        {
            return pfi_codigo;
        }

        set
        {
            pfi_codigo = value;
        }
    }

    public string Pfi_cpf
    {
        get
        {
            return pfi_cpf;
        }

        set
        {
            pfi_cpf = value;
        }
    }

    public DateTime Pfi_data_nascimento
    {
        get
        {
            return pfi_data_nascimento;
        }

        set
        {
            pfi_data_nascimento = value;
        }
    }
}