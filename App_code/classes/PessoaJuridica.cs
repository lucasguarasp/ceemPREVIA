﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de PessoaJuridica
/// </summary>
public class PessoaJuridica: Cliente
{
    private int pju_codigo;
    private string pju_cnpj;
    private string pju_inscricao_estadual;
    private string pju_razao_social;
    private string pju_nome_fantasia;


    public PessoaJuridica()
    {
        Cli_tipo = "J";
    }

    public int Pju_codigo
    {
        get
        {
            return pju_codigo;
        }

        set
        {
            pju_codigo = value;
        }
    }

    public string Pju_cnpj
    {
        get
        {
            return pju_cnpj;
        }

        set
        {
            pju_cnpj = value;
        }
    }

    public string Pju_inscricao_estadual
    {
        get
        {
            return pju_inscricao_estadual;
        }

        set
        {
            pju_inscricao_estadual = value;
        }
    }

    public string Pju_razao_social
    {
        get
        {
            return pju_razao_social;
        }

        set
        {
            pju_razao_social = value;
        }
    }

    public string Pju_nome_fantasia
    {
        get
        {
            return pju_nome_fantasia;
        }

        set
        {
            pju_nome_fantasia = value;
        }
    }
}