﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for TipoUsuario
/// </summary>
public class TipoUsuario
{
    private int idTipoUsuario;
    private string descricao;
    
    public int IdTipoUsuario
    {
        get
        {
            return idTipoUsuario;
        }

        set
        {
            idTipoUsuario = value;
        }
    }

    public string Descricao
    {
        get
        {
            return descricao;
        }

        set
        {
            descricao = value;
        }
    }
}