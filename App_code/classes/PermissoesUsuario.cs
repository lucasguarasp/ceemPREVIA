﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de PermissoesUsuario
/// </summary>
public class PermissoesUsuario
{
    public PermissoesUsuario()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private Usuario usuario;

    private List<Permissoes> permissoes;

    public Usuario Usuario {
        get {
            return usuario;
        }

        set {
            usuario = value;
        }
    }

    public List<Permissoes> Permissoes {
        get {
            return permissoes;
        }

        set {
            permissoes = value;
        }
    }
}