﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de PermissoesTipoUsuario
/// </summary>
public class PermissoesTipoUsuario
{
    public PermissoesTipoUsuario()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private TipoUsuario tipoUsuario;

    private List<Permissoes> permissoes;

    public TipoUsuario TipoUsuario {
        get {
            return tipoUsuario;
        }

        set {
            tipoUsuario = value;
        }
    }

    public List<Permissoes> Permissoes {
        get {
            return permissoes;
        }

        set {
            permissoes = value;
        }
    }
}