﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de LocalInstalacao
/// </summary>
public class LocalInstalacao
{
    public LocalInstalacao()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private int loc_codigo;

    private string loc_nome;

    private string loc_descricao;

    private DateTime loc_dt_instalacao;

    private Cliente cliente;

    public int Loc_codigo {
        get {
            return loc_codigo;
        }

        set {
            loc_codigo = value;
        }
    }

    public string Loc_nome {
        get {
            return loc_nome;
        }

        set {
            loc_nome = value;
        }
    }

    public string Loc_descricao {
        get {
            return loc_descricao;
        }

        set {
            loc_descricao = value;
        }
    }

    public DateTime Loc_dt_instalacao {
        get {
            return loc_dt_instalacao;
        }

        set {
            loc_dt_instalacao = value;
        }
    }

    public Cliente Cliente {
        get {
            return cliente;
        }

        set {
            cliente = value;
        }
    }
}