﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de BandeiraTarifaria
/// </summary>
public class BandeiraTarifaria
{
    public BandeiraTarifaria()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private int ban_codigo;

    private string ban_nome;

    private Cliente cliente;

    public int Ban_codigo {
        get {
            return ban_codigo;
        }

        set {
            ban_codigo = value;
        }
    }

    public string Ban_nome {
        get {
            return ban_nome;
        }

        set {
            ban_nome = value;
        }
    }

    public Cliente Cliente {
        get {
            return cliente;
        }

        set {
            cliente = value;
        }
    }
}