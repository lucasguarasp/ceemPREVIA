﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Telefone
/// </summary>
public class Telefone
{
    private int tel_codigo;
    private string tel_celular;
    private string tel_fixo;
    private string tel_comercial;
    private string tel_recado;
    private DateTime tel_dt_mudanca_tel;
    private Cliente cliente;

    public int Tel_codigo
    {
        get
        {
            return tel_codigo;
        }

        set
        {
            tel_codigo = value;
        }
    }

    public string Tel_celular
    {
        get
        {
            return tel_celular;
        }

        set
        {
            tel_celular = value;
        }
    }

    public string Tel_fixo
    {
        get
        {
            return tel_fixo;
        }

        set
        {
            tel_fixo = value;
        }
    }

    public string Tel_comercial
    {
        get
        {
            return tel_comercial;
        }

        set
        {
            tel_comercial = value;
        }
    }

    public string Tel_recado
    {
        get
        {
            return tel_recado;
        }

        set
        {
            tel_recado = value;
        }
    }

    public DateTime Tel_dt_mudanca_tel
    {
        get
        {
            return tel_dt_mudanca_tel;
        }

        set
        {
            tel_dt_mudanca_tel = value;
        }
    }

    public global::Cliente Cliente {
        get {
            return cliente;
        }

        set {
            cliente = value;
        }
    }
}