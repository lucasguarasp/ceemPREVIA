﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Permissoes
/// </summary>
public class Permissoes
{
    public Permissoes()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private int mod_codigo;

    private string mod_name;

    private string mod_descricao;

    private DateTime mod_dt_autorizacao;

    public int Mod_codigo {
        get {
            return mod_codigo;
        }

        set {
            mod_codigo = value;
        }
    }

    public string Mod_name {
        get {
            return mod_name;
        }

        set {
            mod_name = value;
        }
    }

    public string Mod_descricao {
        get {
            return mod_descricao;
        }

        set {
            mod_descricao = value;
        }
    }

    public DateTime Mod_dt_autorizacao {
        get {
            return mod_dt_autorizacao;
        }

        set {
            mod_dt_autorizacao = value;
        }
    }
}