﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Equipamento
/// </summary>
public class Equipamento
{
    public Equipamento()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private int equ_id;

    private DateTime equ_data_cadastro;

    private Cliente cliente;

    private LocalInstalacao local;

    public int Equ_id {
        get {
            return equ_id;
        }

        set {
            equ_id = value;
        }
    }

    public DateTime Equ_data_cadastro {
        get {
            return equ_data_cadastro;
        }

        set {
            equ_data_cadastro = value;
        }
    }

    public Cliente Cliente {
        get {
            return cliente;
        }

        set {
            cliente = value;
        }
    }

    public LocalInstalacao Local {
        get {
            return local;
        }

        set {
            local = value;
        }
    }
}