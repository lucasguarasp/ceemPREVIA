﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Endereco
/// </summary>
public class Endereco
{
    
    private int end_codigo;
    private string end_logradouro;
    private string end_tipo_logradouro;
    private string end_numero;
    private string end_complemento;
    private string end_bairro;
    private string end_cidade;
    private string end_estado;
    private string end_cep;
    private string end_dt_mudanca_end;
    private Cliente cliente;

    public int End_codigo
    {
        get
        {
            return end_codigo;
        }

        set
        {
            end_codigo = value;
        }
    }

    public string End_logradouro
    {
        get
        {
            return end_logradouro;
        }

        set
        {
            end_logradouro = value;
        }
    }

    public string End_tipo_logradouro
    {
        get
        {
            return end_tipo_logradouro;
        }

        set
        {
            end_tipo_logradouro = value;
        }
    }

    public string End_numero
    {
        get
        {
            return end_numero;
        }

        set
        {
            end_numero = value;
        }
    }

    public string End_complemento
    {
        get
        {
            return end_complemento;
        }

        set
        {
            end_complemento = value;
        }
    }

    public string End_bairro
    {
        get
        {
            return end_bairro;
        }

        set
        {
            end_bairro = value;
        }
    }

    public string End_cidade
    {
        get
        {
            return end_cidade;
        }

        set
        {
            end_cidade = value;
        }
    }

    public string End_estado
    {
        get
        {
            return end_estado;
        }

        set
        {
            end_estado = value;
        }
    }

    public string End_cep
    {
        get
        {
            return end_cep;
        }

        set
        {
            end_cep = value;
        }
    }

    public string End_dt_mudanca_end
    {
        get
        {
            return end_dt_mudanca_end;
        }

        set
        {
            end_dt_mudanca_end = value;
        }
    }

    public global::Cliente Cliente {
        get {
            return cliente;
        }

        set {
            cliente = value;
        }
    }
}