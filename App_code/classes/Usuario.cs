﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Usuario
/// </summary>
public class Usuario
{
    private int codigo;
    private string nome;
    private string login;
    private string senha;
    private bool status;
    private DateTime dataCadastro;
    private TipoUsuario fkTipoUsuario;
    
    public string Nome
    {
        get { return nome; }
        set { nome = value; }
    }

    public string Login
    {
        get { return login; }
        set { login = value; }
    }

    public string Senha
    {
        get { return senha; }
        set { senha = value; }
    }


    public bool Status
    {
        get { return status; }
        set { status = value; }
    }

    public DateTime DataCadastro
    {
        get { return dataCadastro; }
        set { dataCadastro = value; }
    }

     public global::TipoUsuario FkTipoUsuario
    {
        get
        {
            return fkTipoUsuario;
        }

        set
        {
            fkTipoUsuario = value;
        }
    }

    public int Codigo
    {
        get
        {
            return codigo;
        }

        set
        {
            codigo = value;
        }
    }
}