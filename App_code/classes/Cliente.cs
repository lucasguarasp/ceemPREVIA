﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Cliente
/// </summary>
public class Cliente
{
    private int cli_codigo;
    private string cli_tipo;
    private string cli_nome;
    private string cli_email;
    private DateTime cli_data_vencimento;
    private char cli_status;
    private DateTime cli_data_cadastro;

    public int Cli_codigo
    {
        get
        {
            return cli_codigo;
        }

        set
        {
            cli_codigo = value;
        }
    }

    public string Cli_tipo
    {
        get
        {
            return cli_tipo;
        }

        set
        {
            cli_tipo = value;
        }
    }

    public string Cli_nome
    {
        get
        {
            return cli_nome;
        }

        set
        {
            cli_nome = value;
        }
    }

    public string Cli_email
    {
        get
        {
            return cli_email;
        }

        set
        {
            cli_email = value;
        }
    }

    public DateTime Cli_data_vencimento
    {
        get
        {
            return cli_data_vencimento;
        }

        set
        {
            cli_data_vencimento = value;
        }
    }

    public char Cli_status
    {
        get
        {
            return cli_status;
        }

        set
        {
            cli_status = value;
        }
    }

    public DateTime Cli_data_cadastro
    {
        get
        {
            return cli_data_cadastro;
        }

        set
        {
            cli_data_cadastro = value;
        }
    }
}