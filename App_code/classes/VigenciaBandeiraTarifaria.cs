﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de VigenciaBandeiraTarifaria
/// </summary>
public class VigenciaBandeiraTarifaria
{
    public VigenciaBandeiraTarifaria()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private int dat_codigo;

    private DateTime dat_inicio;

    private DateTime dat_fim;

    private bool dat_verao;

    private HistoricoBandeiraTarifaria historico_bandeira;

    public int Dat_codigo {
        get {
            return dat_codigo;
        }

        set {
            dat_codigo = value;
        }
    }

    public DateTime Dat_inicio {
        get {
            return dat_inicio;
        }

        set {
            dat_inicio = value;
        }
    }

    public DateTime Dat_fim {
        get {
            return dat_fim;
        }

        set {
            dat_fim = value;
        }
    }

    public bool Dat_verao {
        get {
            return dat_verao;
        }

        set {
            dat_verao = value;
        }
    }

    public HistoricoBandeiraTarifaria Historico_bandeira {
        get {
            return historico_bandeira;
        }

        set {
            historico_bandeira = value;
        }
    }
}