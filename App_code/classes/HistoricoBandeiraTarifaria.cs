﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de HistoricoBandeiraTarifaria
/// </summary>
public class HistoricoBandeiraTarifaria
{
    public HistoricoBandeiraTarifaria()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    private int hba_codigo;

    private double hba_valor;

    private double hba_limite_kwh;

    private DateTime hba_dt_cadastro;

    private bool hba_is_ativa;

    private BandeiraTarifaria bandeira;

    public int Hba_codigo {
        get {
            return hba_codigo;
        }

        set {
            hba_codigo = value;
        }
    }

    public double Hba_valor {
        get {
            return hba_valor;
        }

        set {
            hba_valor = value;
        }
    }

    public double Hba_limite_kwh {
        get {
            return hba_limite_kwh;
        }

        set {
            hba_limite_kwh = value;
        }
    }

    public DateTime Hba_dt_cadastro {
        get {
            return hba_dt_cadastro;
        }

        set {
            hba_dt_cadastro = value;
        }
    }

    public bool Hba_is_ativa {
        get {
            return hba_is_ativa;
        }

        set {
            hba_is_ativa = value;
        }
    }

    public BandeiraTarifaria Bandeira {
        get {
            return bandeira;
        }

        set {
            bandeira = value;
        }
    }
}