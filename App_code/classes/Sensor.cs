﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de Sensor
/// </summary>
public class Sensor
{
    public Sensor()
    {
        //
        // TODO: Adicionar lógica do construtor aqui
        //
    }

    public Sensor(Equipamento equipamento)
    {
        this.Equipamento = equipamento;
    }

    private int sen_id;

    private DateTime sen_dt_cadastro;

    private Equipamento equipamento;

    private LocalInstalacao local;

    public int Sen_id {
        get {
            return sen_id;
        }

        set {
            sen_id = value;
        }
    }

    public DateTime Sen_dt_cadastro {
        get {
            return sen_dt_cadastro;
        }

        set {
            sen_dt_cadastro = value;
        }
    }

    public Equipamento Equipamento {
        get {
            return equipamento;
        }

        set {
            equipamento = value;
        }
    }

    public LocalInstalacao Local {
        get {
            return local;
        }

        set {
            local = value;
        }
    }
}