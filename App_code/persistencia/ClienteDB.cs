﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// Descrição resumida de ClienteDB
/// </summary>
public class ClienteDB
{
    public static int InsertOrUpdate(Cliente cliente, Endereco endereco, Telefone telefone)
    {
        bool update = cliente.Cli_codigo != 0;
        int retorno = 0;

        const string INSERT_CLIENTE = @"INSERT INTO cli_cliente
            (cli_tipo, cli_nome, cli_email, cli_data_vencimento, cli_status, cli_dt_cadastro)
            VALUES(?cli_tipo, ?cli_nome, ?cli_email, ?cli_data_vencimento, ?cli_status, ?cli_dt_cadastro);
            SELECT LAST_INSERT_ID();";

        const string UPDATE_CLIENTE = @"UPDATE cli_cliente
            SET cli_tipo = ?cli_tipo, cli_nome = ?cli_nome, cli_email = ?cli_email,
            cli_data_vencimento = ?cli_data_vencimento, cli_status = ?cli_status";

        const string INSERT_PESSOA_FISICA = @"INSERT INTO pfi_pessoa_fisica
            (pfi_cpf, pfi_data_nascimento, cli_codigo)
            VALUES(?pfi_cpf, ?pfi_data_nascimento, ?cli_codigo);";

        const string UPDATE_PESSOA_FISICA = @"UPDATE pfi_pessoa_fisica
            SET pfi_cpf = ?pfi_cpf, pfi_data_nascimento = ?pfi_data_nascimento, cli_codigo = ?cli_codigo";

        const string INSERT_PESSOA_JURIDICA = @"INSERT INTO pju_pessoa_juridica
            (pju_cnpj, pju_inscricao_estadual, pju_razao_social, pju_nome_fantasia, cli_codigo)
            VALUES(?pju_cnpj, ?pju_inscricao_estadual, ?pju_razao_social,? pju_nome_fantasia, ?cli_codigo);";

        const string UPDATE_PESSOA_JURIDICA = @"UPDATE pju_pessoa_juridica
            SET pju_cnpj = ?pju_cnpj, pju_inscricao_estadual = ?pju_inscricao_estadual,
            pju_razao_social = ?pju_razao_social, pju_nome_fantasia = ?pju_nome_fantasia, cli_codigo = ?cli_codigo";

        const string INSERT_TELEFONE = @"INSERT INTO tel_telefone
            (tel_celular, tel_fixo, tel_comercial, tel_recado, tel_dt_mudanca_tel, cli_codigo)
            VALUES(?tel_celular, ?tel_fixo, ?tel_comercial, ?tel_recado, ?tel_dt_mudanca_tel, ?cli_codigo);";

        const string UPDATE_TELEFONE = @"UPDATE tel_telefone
            SET tel_celular = ?tel_celular, tel_fixo = ?tel_fixo, tel_comercial = ?tel_comercial,
            tel_recado = ?tel_recado, tel_dt_mudanca_tel = ?tel_dt_mudanca_tel, cli_codigo = ?cli_codigo";

        const string INSERT_ENDERECO = @"INSERT INTO end_endereco
            (end_logradouro, end_tipo_logradouro, end_numero, end_complemento,
                end_bairro, end_cidade, end_estado, end_cep, end_dt_mudanca_end, cli_codigo)
            VALUES(?end_logradouro, ?end_tipo_logradouro, ?end_numero, ?end_complemento,
                ?end_bairro, ?end_cidade, ?end_estado, ?end_cep, ?end_dt_mudanca_end, ?cli_codigo);";

        const string UPDATE_ENDERECO = @"UPDATE end_endereco
            SET end_logradouro = ?end_logradouro, end_tipo_logradouro = ?end_tipo_logradouro,
            end_numero = ?end_numero, end_complemento = ?end_complemento,
            end_bairro = ?end_bairro, end_cidade = ?end_cidade, end_estado = ?end_estado,
            end_cep = ?end_cep, end_dt_mudanca_end = ?end_dt_mudanca_end, cli_codigo = ?cli_codigo";

        const string WHERE = " WHERE cli_codigo = ?cli_codigo;";

        try {
            using (IDbConnection conn = Mapped.Connection()) {
                IDbTransaction transacao = conn.BeginTransaction();

                // Cliente
                IDbCommand cmd = Mapped.Command(update ? UPDATE_CLIENTE + WHERE : INSERT_CLIENTE, conn);
                cmd.Parameters.Add(Mapped.Parameter("?cli_tipo", cliente.Cli_tipo));
                cmd.Parameters.Add(Mapped.Parameter("?cli_nome", cliente.Cli_nome));
                cmd.Parameters.Add(Mapped.Parameter("?cli_email", cliente.Cli_email));
                cmd.Parameters.Add(Mapped.Parameter("?cli_data_vencimento", cliente.Cli_data_vencimento));
                cmd.Parameters.Add(Mapped.Parameter("?cli_status", cliente.Cli_status));

                if (update)
                {
                    cmd.Parameters.Add(
                        Mapped.Parameter("?cli_codigo", cliente.Cli_codigo));
                }
                else
                {
                    cmd.Parameters.Add(
                        Mapped.Parameter("?cli_dt_cadastro", DateTime.Now));
                }
                cmd.ExecuteNonQuery();

                // Para o Insert
                if (!update)
                {
                    cliente.Cli_codigo = Convert.ToInt32(cmd.ExecuteScalar());
                    retorno = cliente.Cli_codigo;
                }
                telefone.Cliente = cliente;
                endereco.Cliente = cliente;
                
                // Tipo pessoa
                if (cliente is PessoaFisica)
                {
                    PessoaFisica pf = cliente as PessoaFisica;
                    cmd = Mapped.Command(update ? UPDATE_PESSOA_FISICA + WHERE : INSERT_PESSOA_FISICA, conn);
                    cmd.Parameters.Add(Mapped.Parameter("?pfi_cpf", pf.Pfi_cpf));
                    cmd.Parameters.Add(Mapped.Parameter("?pfi_data_nascimento", pf.Pfi_data_nascimento));
                    cmd.Parameters.Add(Mapped.Parameter("?cli_codigo", pf.Cli_codigo));
                }
                else if (cliente is PessoaJuridica)
                {
                    PessoaJuridica pj = cliente as PessoaJuridica;
                    cmd = Mapped.Command(update ? UPDATE_PESSOA_JURIDICA + WHERE : INSERT_PESSOA_JURIDICA, conn);
                    cmd.Parameters.Add(Mapped.Parameter("?pju_cnpj", pj.Pju_cnpj));
                    cmd.Parameters.Add(Mapped.Parameter("?pju_inscricao_estadual", pj.Pju_inscricao_estadual));
                    cmd.Parameters.Add(Mapped.Parameter("?pju_razao_social", pj.Pju_razao_social));
                    cmd.Parameters.Add(Mapped.Parameter("?pju_nome_fantasia", pj.Pju_nome_fantasia));
                    cmd.Parameters.Add(Mapped.Parameter("?cli_codigo", pj.Cli_codigo));
                } else
                {
                    throw new FormatException(
                        "O objeto deve ser ou PessoaFisica ou PessoaJuridica");
                }
                cmd.ExecuteNonQuery();

                // Telefone
                cmd = Mapped.Command(update ? UPDATE_TELEFONE + WHERE : INSERT_TELEFONE, conn);
                cmd.Parameters.Add(Mapped.Parameter("?tel_celular", telefone.Tel_celular));
                cmd.Parameters.Add(Mapped.Parameter("?tel_fixo", telefone.Tel_fixo));
                cmd.Parameters.Add(Mapped.Parameter("?tel_comercial", telefone.Tel_comercial));
                cmd.Parameters.Add(Mapped.Parameter("?tel_recado", telefone.Tel_recado));
                cmd.Parameters.Add(Mapped.Parameter("?tel_dt_mudanca_tel", DateTime.Now));
                cmd.Parameters.Add(Mapped.Parameter("?cli_codigo", telefone.Cliente.Cli_codigo));
                cmd.ExecuteNonQuery();

                cmd = Mapped.Command(update? UPDATE_ENDERECO + WHERE:INSERT_ENDERECO, conn);
                cmd.Parameters.Add(Mapped.Parameter("?end_logradouro", endereco.End_logradouro));
                cmd.Parameters.Add(Mapped.Parameter("?end_tipo_logradouro", endereco.End_tipo_logradouro));
                cmd.Parameters.Add(Mapped.Parameter("?end_numero", endereco.End_numero));
                cmd.Parameters.Add(Mapped.Parameter("?end_complemento", endereco.End_complemento));
                cmd.Parameters.Add(Mapped.Parameter("?end_bairro", endereco.End_bairro));
                cmd.Parameters.Add(Mapped.Parameter("?end_cidade", endereco.End_cidade));
                cmd.Parameters.Add(Mapped.Parameter("?end_estado", endereco.End_estado));
                cmd.Parameters.Add(Mapped.Parameter("?end_cep", endereco.End_cep));
                cmd.Parameters.Add(Mapped.Parameter("?end_dt_mudanca_end", DateTime.Now));
                cmd.Parameters.Add(Mapped.Parameter("?cli_codigo", endereco.Cliente.Cli_codigo));
                cmd.ExecuteNonQuery();

                transacao.Commit();
            }
        }
        catch (Exception e)
        {
            throw e;
            retorno = -2;
        }
        return retorno;
    }
    public static DataSet Select(int codigo)
    {
        return SelectAll(new List<int> { codigo });
    }

    /*
     * Exemplo de chamada: ClienteDB.SelectAll(codigos: new List<int> { 1, 2 });
     * 
     * Sem nenhum parametro o metodo retorna todos os clientes
     * 
     */
    public static DataSet SelectAll(List<int> codigos = null)
    {
        string select = @"
            SELECT
	            -- CLIENTE
	            cli.cli_codigo, 
                CASE cli_tipo WHEN 'J' THEN 'JURIDICA' ELSE 'FISICA' END cli_tipo,
                cli_nome, cli_email,
                FORMATO_DATA_PADRAO(cli_data_vencimento) cli_data_vencimento,
                cli_status, TO_NOME_STATUS_CLIENTE(cli_status) nome_status,
                -- PESSOA FISICA
                pfi_cpf,
                FORMATO_DATA_PADRAO(cli_dt_cadastro) cli_dt_cadastro, 
                FORMATO_DATA_PADRAO(pfi_data_nascimento) pfi_data_nascimento,
                -- PESSOA JURIDICA
	            pju_cnpj, pju_inscricao_estadual, pju_razao_social, pju_nome_fantasia,
                -- TELEFONE
                tel_celular, tel_fixo, tel_comercial, tel_recado,
                FORMATO_DATA_PADRAO(tel_dt_mudanca_tel) tel_dt_mudanca_tel,
                -- ENDERECO
                end_logradouro, end_tipo_logradouro, end_numero, end_complemento, 
                end_bairro, end_cidade, end_estado, end_cep,
                FORMATO_DATA_PADRAO(end_dt_mudanca_end) end_dt_mudanca_end
            FROM cli_cliente cli
                INNER JOIN end_endereco end ON end.cli_codigo = cli.cli_codigo
                INNER JOIN tel_telefone tel ON tel.cli_codigo = cli.cli_codigo
                LEFT JOIN pju_pessoa_juridica pju ON pju.cli_codigo = cli.cli_codigo
                LEFT JOIN pfi_pessoa_fisica pfi ON pfi.cli_codigo = cli.cli_codigo";

        if (codigos != null) {
            select += " WHERE cli.cli_codigo IN(";

            int nCodigo = 0;
            select += "?cli_codigo_" + nCodigo++;
            while (nCodigo < codigos.Count)
            {
                select += ",?cli_codigo_" + nCodigo++;
            }
            select += ")";

            select += " AND ";
        } else
        {
            select += " WHERE ";
        }

        select += "cli.cli_status != 'P' ";
        select += " ORDER BY cli.cli_codigo DESC";

        DataSet ds = new DataSet();
        using (IDbConnection conn = Mapped.Connection())
        {
            IDbCommand cmd = Mapped.Command(select, conn);

            if (codigos != null)
            {
                int nCodigo = 0;
                foreach (int codigo in codigos)
                {
                    cmd.Parameters.Add(
                        Mapped.Parameter("?cli_codigo_" + nCodigo++, codigo));
                }
            }
            IDataAdapter dataAdapter = Mapped.Adapter(cmd);
            dataAdapter.Fill(ds);
        }
        return ds;
    }
}