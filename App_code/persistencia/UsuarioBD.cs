﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de UsuarioBD
/// </summary>
public class UsuarioBD
{
    public static int Insert(Usuario usuario)
    {
        int retorno = 0;
        try
        {
            IDbConnection objConexao;  // Abre a conexao
            IDbCommand objCommand;  // Cria o comando
            string sql = "INSERT INTO usu_usuario (usu_nome, usu_login, usu_senha, usu_status, usu_dt_cadastro, tip_tipo_usuario_tip_codigo) VALUES (?usu_nome, ?usu_login, ?usu_senha, ?usu_status, ?usu_dt_cadastro, ?tip_tipo_usuario_tip_codigo)";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?usu_nome", usuario.Nome));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_login", usuario.Login));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_senha", usuario.Senha));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_status", usuario.Status));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_dt_cadastro", usuario.DataCadastro));
            objCommand.Parameters.Add(Mapped.Parameter("?tip_tipo_usuario_tip_codigo", usuario.FkTipoUsuario.IdTipoUsuario));
            objCommand.ExecuteNonQuery();   // utilizado quando cdigo não tem retorno, como seria o caso do SELECT
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            retorno = -2;
        }
        return retorno;
    }

    public static int Insert(TipoUsuario tipo)
    {
        int retorno = 0;
        try
        {
            IDbConnection objConexao;  // Abre a conexao
            IDbCommand objCommand;  // Cria o comando
            string sql = "INSERT INTO tip_tipo_usuario (tip_descricao) VALUES (?tip_descricao)";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?tip_descricao", tipo.Descricao));
            objCommand.ExecuteNonQuery();   // utilizado quando cdigo não tem retorno, como seria o caso do SELECT
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            retorno = -2;
        }
        return retorno;
    }

    public static DataSet SelectAdm()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT usu_codigo, usu_nome, usu_login, CASE WHEN usu_status = 'false' THEN 'Bloqueado' ELSE 'Desbloquado' END usu_status, " +
            "(Date_Format(usu_dt_cadastro,'%d/%m/%Y')) As Date , tip_descricao " +
            "FROM usu_usuario INNER JOIN tip_tipo_usuario " +
            "ON tip_tipo_usuario_tip_codigo = tip_codigo " +
            "where tip_tipo_usuario_tip_codigo = 1", conexao: objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public static DataSet SelectSuporte()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT usu_codigo, usu_nome, usu_login, CASE WHEN usu_status = 'false' THEN 'Bloqueado' ELSE 'Desbloquado' END usu_status, " +
            "(Date_Format(usu_dt_cadastro,'%d/%m/%Y')) As Date , tip_descricao " +
            "FROM usu_usuario INNER JOIN tip_tipo_usuario " +
            "ON tip_tipo_usuario_tip_codigo = tip_codigo " +
            "where tip_tipo_usuario_tip_codigo = 2", conexao: objConnection);

        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public static DataSet SelectCliente()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT usu_codigo, usu_nome, usu_login, CASE WHEN usu_status = 'false' THEN 'Bloqueado' ELSE 'Desbloquado' END usu_status, " +
            "(FORMATO_DATA_PADRAO(usu_dt_cadastro) As Date , tip_descricao " +
            "FROM usu_usuario INNER JOIN tip_tipo_usuario " +
            "ON tip_tipo_usuario_tip_codigo = tip_codigo " +
            "where tip_tipo_usuario_tip_codigo = 3", conexao: objConnection);

        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public static DataSet SelectPre()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT usu_codigo, usu_nome, usu_login, CASE WHEN usu_status = 'false' THEN 'Bloqueado' ELSE 'Desbloquado' END usu_status, " +
            "(Date_Format(usu_dt_cadastro,'%d/%m/%Y')) As Date , tip_descricao " +
            "FROM usu_usuario INNER JOIN tip_tipo_usuario " +
            "ON tip_tipo_usuario_tip_codigo = tip_codigo " +
            "where tip_tipo_usuario_tip_codigo = 4", conexao: objConnection);

        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public static DataSet SelectEquipamentos()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM equ_equipamento", conexao: objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public static DataSet SelectSensores()
    {

        DataSet ds = new DataSet();
        IDbConnection objConnection;
        IDbCommand objCommand;
        IDataAdapter objDataAdapter;
        objConnection = Mapped.Connection();
        objCommand = Mapped.Command("SELECT * FROM sen_sensor", conexao: objConnection);
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds); // O objeto DataAdapter vai preencher o 
                                 // DataSet com os dados do BD, O método Fill é o responsável por 
                                 // preencher o DataSet
        objConnection.Close();
        objCommand.Dispose();
        objConnection.Dispose();
        return ds;
    }

    public static DataSet SelectLOGIN(string login, string senha)
    {
        DataSet ds = new DataSet(); IDbConnection objConexao; IDbCommand objCommand;
        IDataAdapter objDataAdapter; string sql = "select * from usu_usuario where usu_login = ?usu_login and usu_senha = ?usu_senha";
        objConexao = Mapped.Connection();
        objCommand = Mapped.Command(sql, objConexao);
        objCommand.Parameters.Add(Mapped.Parameter("?usu_login", login));
        objCommand.Parameters.Add(Mapped.Parameter("?usu_senha", senha));
        objDataAdapter = Mapped.Adapter(objCommand);
        objDataAdapter.Fill(ds);
        objConexao.Close();
        objConexao.Dispose();
        objCommand.Dispose();
        return ds;
    }

    public static int Update(Usuario usuario)
    {

        int retorno = 0;
        try
        {
            IDbConnection objConexao;  // Abre a conexao
            IDbCommand objCommand;  // Cria o comando
            string sql = "UPDATE usu_usuario SET usu_nome = ?usu_nome, usu_login = ?usu_login, tip_tipo_usuario_tip_codigo = ?tip_tipo_usuario_tip_codigo WHERE usu_codigo = ?usu_codigo";
            objConexao = Mapped.Connection();
            objCommand = Mapped.Command(sql, objConexao);
            objCommand.Parameters.Add(Mapped.Parameter("?usu_nome", usuario.Nome));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_login", usuario.Login));
            //objCommand.Parameters.Add(Mapped.Parameter("?usu_senha", usuario.Senha));
            objCommand.Parameters.Add(Mapped.Parameter("?tip_tipo_usuario_tip_codigo", usuario.FkTipoUsuario.IdTipoUsuario));
            objCommand.Parameters.Add(Mapped.Parameter("?usu_codigo", usuario.Codigo));
            objCommand.ExecuteNonQuery();   // utilizado quando cdigo não tem retorno, como seria o caso do SELECT
            objConexao.Close();
            objCommand.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e)
        {
            retorno = -2;
        }
        return retorno;
    }

    public static int Delete(Usuario usuario)
    {

        int retorno = 0; try
        {
            IDbConnection objConexao; IDbCommand objComando;
            //SINTAXE: "DELETE FROM nomeDaTabela WHERE nomeDaColunaID = ?parametrização"; 
            string sql = "DELETE * FROM usu_usuario WHERE usu_codigo= ?usu_codigo";
            objConexao = Mapped.Connection();
            objComando = Mapped.Command(sql, objConexao);
            objComando.Parameters.Add(Mapped.Parameter("?usu_codigo", usuario.Codigo));
            objComando.ExecuteNonQuery();
            objConexao.Close();
            objComando.Dispose();
            objConexao.Dispose();
        }
        catch (Exception e) { retorno = -2; }
        return retorno;
    }

}
