﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de EquipamentoDB
/// </summary>
public class EquipamentoDB
{
    public static int InsertOrUpdate(
        List<Sensor> sensores, List<Equipamento> equipamentos, List<LocalInstalacao> locais)
    {
        int retorno = 0;
        bool update;

        const string INSERT_SENSOR = @"INSERT INTO sen_sensor
            (sen_dt_cadastro, equ_id, loc_codigo VALUES (?sen_dt_cadastro, ?equ_id, ?loc_codigo);";

        const string UPDATE_SENSOR = @"UPDATE sen_sensor
            SET sen_dt_cadastro = ?sen_dt_cadastro, equ_id = ?equ_id, loc_codigo = ?loc_codigo
            WHERE sen_id = ?sen_id;";

        const string INSERT_EQUIPAMENTO = @"INSERT INTO equ_equipamento
            (equ_dt_cadastro, cli_codigo, loc_codigo)
            VALUES (?equ_dt_cadastro, ?cli_codigo, ?loc_codigo);";

        const string UPDATE_EQUIPAMENTO = @"UPDATE equ_equipamento
            SET equ_dt_cadastro = ?equ_dt_cadastro, cli_codigo = ?cli_codigo,
            loc_codigo = ?loc_codigo
            WHERE equ_id = ?equ_id;";

        const string INSERT_LOCAL = @"INSERT INTO loc_local_instalacao
            (loc_nome, loc_descricao, loc_dt_instalacao, cli_codigo)
            VALUES (?loc_nome, ?loc_descricao, ?loc_dt_instalacao, ?cli_codigo);";

        const string UPDATE_LOCAL = @"UPDATE loc_local_instalacao
            SET  loc_nome = ?loc_nome, loc_descricao = ?loc_descricao,
            loc_dt_instalacao = ?loc_dt_instalacao, cli_codigo = ?cli_codigo);";

        try
        {
            using (IDbConnection conn = Mapped.Connection())
            {
                IDbCommand cmd;
                IDbTransaction transacao = conn.BeginTransaction();

                foreach (LocalInstalacao local in locais)
                {
                    update = local.Loc_codigo != 0;

                    cmd = Mapped.Command(
                        update ? UPDATE_LOCAL : INSERT_LOCAL, conn);
                    cmd.Parameters.Add(
                        Mapped.Parameter("?loc_nome", local.Loc_nome));
                    cmd.Parameters.Add(
                        Mapped.Parameter("?loc_descricao", local.Loc_descricao));
                    cmd.Parameters.Add(
                        Mapped.Parameter("?loc_dt_instalacao", local.Loc_dt_instalacao));
                    cmd.Parameters.Add(
                        Mapped.Parameter("?cli_codigo", local.Cliente.Cli_codigo));

                    if (update)
                    {
                        cmd.Parameters.Add(
                            Mapped.Parameter("?loc_codigo", local.Loc_codigo));
                    }
                    cmd.ExecuteNonQuery();
                }

                foreach (Equipamento equipamento in equipamentos)
                {
                    update = equipamento.Equ_id != 0;

                    cmd = Mapped.Command(
                        update ? UPDATE_EQUIPAMENTO : INSERT_EQUIPAMENTO, conn);
                    cmd.Parameters.Add(
                        Mapped.Parameter("?equ_dt_cadastro", equipamento.Equ_data_cadastro));
                    cmd.Parameters.Add(
                        Mapped.Parameter("?loc_codigo", equipamento.Local.Loc_codigo));
                    cmd.Parameters.Add(
                        Mapped.Parameter("?cli_codigo", equipamento.Cliente.Cli_codigo));

                    if (update)
                    {
                        cmd.Parameters.Add(
                            Mapped.Parameter("?equ_id", equipamento.Equ_id));
                    }
                    cmd.ExecuteNonQuery();
                }

                foreach (Sensor sensor in sensores)
                {
                    update = sensor.Sen_id != 0;

                    cmd = Mapped.Command(update ? UPDATE_SENSOR : INSERT_SENSOR, conn);
                    cmd.Parameters.Add(Mapped.Parameter("?sen_dt_cadastro", sensor.Sen_dt_cadastro));
                    cmd.Parameters.Add(Mapped.Parameter("?equ_id", sensor.Equipamento.Equ_id));
                    cmd.Parameters.Add(Mapped.Parameter("?loc_codigo", sensor.Local.Loc_codigo));

                    if (update)
                    {
                        cmd.Parameters.Add(Mapped.Parameter("?sen_id", sensor.Sen_id));
                    }
                    cmd.ExecuteNonQuery();
                }
                transacao.Commit();
            }
        }
        catch (Exception e)
        {
            throw e;
            retorno = -2;
        }
        return retorno;
    }

    public static DataSet SelectAll(
            List<int> equ_codigos = null, List<int> sen_codigos = null, List<int> cli_codigos = null)
    {
        string select = @"
            SELECT
	            sen.sen_id,
                loc_sen.loc_nome local_sensor,
	            loc_sen.loc_descricao loc_descricao_sensor,
                loc_sen.loc_dt_instalacao loc_dt_instalacao_sensor,
                equ.equ_id,
	            loc_equ.loc_nome loc_equipamento,
                loc_equ.loc_descricao loc_descricao_equipamento,
                loc_equ.loc_dt_instalacao loc_dt_instalacao_equipamento,
                loc_equ.cli_codigo
            FROM sen_sensor sen
	            INNER JOIN loc_local_instalacao loc_sen ON loc_sen.loc_codigo = sen.loc_codigo
	            INNER JOIN equ_equipamento equ ON equ.equ_id = sen.equ_id
	            INNER JOIN loc_local_instalacao loc_equ ON loc_equ.loc_codigo = equ.loc_codigo";

        if (cli_codigos != null)
        {
            select += " WHERE equ.cli_codigo IN(";

            int nCodigo = 0;
            select += "?cli_codigo_" + nCodigo++;
            while (nCodigo < cli_codigos.Count)
            {
                select += ",?cli_codigo_" + nCodigo++;
            }
            select += ")";
        }

        if (equ_codigos != null)
        {
            select += ((cli_codigos == null) ? " WHERE " : " AND ");
            select += "equ.equ_id IN(";

            int nCodigo = 0;
            select += "?equ_id_" + nCodigo++;
            while (nCodigo < equ_codigos.Count)
            {
                select += ",?equ_id_" + nCodigo++;
            }
            select += ")";
        }

        if (sen_codigos != null)
        {
            select += ((cli_codigos == null
                        && equ_codigos == null) ? " WHERE " : " AND ");

            select += " sen.sen_id IN(";

            int nCodigo = 0;
            select += "?sen_id_" + nCodigo++;
            while (nCodigo < sen_codigos.Count)
            {
                select += ",?sen_id_" + nCodigo++;
            }
            select += ")";
        }

        DataSet ds = new DataSet();
        using (IDbConnection conn = Mapped.Connection())
        {
            IDbCommand cmd = Mapped.Command(select, conn);

            if (cli_codigos != null)
            {
                int nCodigo = 0;
                foreach (int codigo in cli_codigos)
                {
                    cmd.Parameters.Add(
                        Mapped.Parameter("?cli_codigo_" + nCodigo++, codigo));
                }
            }

            if (equ_codigos != null)
            {
                int nCodigo = 0;
                foreach (int codigo in equ_codigos)
                {
                    cmd.Parameters.Add(
                        Mapped.Parameter("?equ_id_" + nCodigo++, codigo));
                }
            }

            if (sen_codigos != null)
            {
                int nCodigo = 0;
                foreach (int codigo in sen_codigos)
                {
                    cmd.Parameters.Add(
                        Mapped.Parameter("?sen_id_" + nCodigo++, codigo));
                }
            }

            IDataAdapter dataAdapter = Mapped.Adapter(cmd);
            dataAdapter.Fill(ds);
        }
        return ds;
    }
}